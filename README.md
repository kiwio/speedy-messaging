# Speedy Messaging #

Speedy Messaging is a high performance event handling network framework. It's purpose is to pass marshalled events from one jvm to another over udp.

## Features ##
 - Using Speedy Messaging, benchmarks have shown it's possible to pass up to 20 million marshalled 40 byte events a second on a single thread (per host). Ideal for high throughput environments.
 - Events are guaranteed to be processed in order they were published.
 - Events are reliable, with resending of missed events on NACK (non ACK) with the throughput benefits of UDP
 - Using the network builder one can go from a simple one to one host map, to a many to many event model.
 - Garbage free 
 - Bounded queue to prevent blowing up the heap.
 

## Pre configuration ##
The socket receive buffers Max size must be increased. Like so
 
 ```
    sysctl -w net.core.rmem_max=33554432 
 ```

## Example ##
```java
public class SimpleExample {

    private static final String EVENT_KEY = "eventKey";
    private static final String CHANNEL_NAME = "simplePubSub";

    public static void main(String[] args) throws InterruptedException, IOException {

        ExampleNetwork exampleNetwork = new ExampleNetwork();

        SpeedyMessagingImpl publisher = exampleNetwork.buildPublisher();
        SpeedyMessagingImpl subscriber = exampleNetwork.buildSubscriber();

        ExampleSubscriber.addSubscriptionHandler(subscriber);

        // start messengers in different threads as they block until they detect each other
        parallel(subscriber::start, publisher::start);

        ExamplePublisher.sendMessages(publisher);

        publisher.close();
        subscriber.close();
    }

    private static class ExampleNetwork {

        private final SpeedyHost publisherHost = new SpeedyHost("publisher", "127.0.0.1", 8543);
        private SpeedyHost susbcriberHost = new SpeedyHost("subscriber", "127.0.0.1", 8544);
        public SpeedyNetwork exampleNetwork = new SpeedyNetworkBuilder()
                .addChannel(
                        SpeedyChannel.newChannel(CHANNEL_NAME)
                        .withPublishers(publisherHost)
                        .forKeys(EVENT_KEY)
                        .toSubscribers(susbcriberHost))
                .buildNetwork();

        public SpeedyMessagingImpl buildPublisher() {
            return exampleNetwork.createInstanceBuilderFromPerspective(publisherHost).build();
        }

        public SpeedyMessagingImpl buildSubscriber() {
            return exampleNetwork.createInstanceBuilderFromPerspective(susbcriberHost).build();
        }
    }

    private static class ExampleSubscriber {
        public static void addSubscriptionHandler(final SpeedyMessagingImpl subscriber) {
            subscriber.subscribe(EVENT_KEY, message -> {

                byte[] bytes = new byte[message.remaining()];
                message.get(bytes);
                System.out.println(new String(bytes));
            });
        }
    }

    private static class ExamplePublisher {
        public static void sendMessages(SpeedyMessagingImpl publisher) {
            for (int i = 0; i < 10; i++) {
                publisher.publish(EVENT_KEY, ("hello, this is message " + i).getBytes(Charset.forName("UTF-8")));
            }
        }
    }
}
```

#### Output ####
```
   received value: 0
   received value: 1
   received value: 2
   received value: 3
   received value: 4
   received value: 5
   received value: 6
   received value: 7
   received value: 8
   received value: 9
```


## Performance results ##

Run on an Intel Core i7-6700K CPU 4.00GHz. This benchmarks is testing raw throughput of sending 40 byte messages.

```
published 100000000 messages in 6827870948ns. That's an average of 68ns a message which is 14705882msg/s
published 100000000 messages in 4873387666ns. That's an average of 48ns a message which is 20833333msg/s
published 100000000 messages in 5549563169ns. That's an average of 55ns a message which is 18181818msg/s
published 100000000 messages in 5699795627ns. That's an average of 56ns a message which is 17857142msg/s
published 100000000 messages in 5320560457ns. That's an average of 53ns a message which is 18867924msg/s
published 100000000 messages in 5007084436ns. That's an average of 50ns a message which is 20000000msg/s
published 100000000 messages in 4886708412ns. That's an average of 48ns a message which is 20833333msg/s
published 100000000 messages in 4821040311ns. That's an average of 48ns a message which is 20833333msg/s
published 100000000 messages in 4871664512ns. That's an average of 48ns a message which is 20833333msg/s
published 100000000 messages in 4988754892ns. That's an average of 49ns a message which is 20408163msg/s
published 100000000 messages in 4911216134ns. That's an average of 49ns a message which is 20408163msg/s
published 100000000 messages in 4847772972ns. That's an average of 48ns a message which is 20833333msg/s
published 100000000 messages in 4755935436ns. That's an average of 47ns a message which is 21276595msg/s
published 100000000 messages in 4857958101ns. That's an average of 48ns a message which is 20833333msg/s
published 100000000 messages in 4946235516ns. That's an average of 49ns a message which is 20408163msg/s
published 100000000 messages in 5085721413ns. That's an average of 50ns a message which is 20000000msg/s
published 100000000 messages in 5423139726ns. That's an average of 54ns a message which is 18518518msg/s
```