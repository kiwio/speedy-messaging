#!/usr/bin/env bash

gcc -fPIC -I"$JAVA_HOME/include" -I"$JAVA_HOME/include/linux" -shared -o liblinuxdatachannel.so io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel.c
mv liblinuxdatachannel.so ../../../../../../../lib/liblinuxdatachannel.so