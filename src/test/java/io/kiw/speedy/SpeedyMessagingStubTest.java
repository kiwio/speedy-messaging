package io.kiw.speedy;

import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetwork;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;
import io.kiw.speedy.helper.ByteBufferHelper;
import io.kiw.speedy.wiring.StubRouter;
import io.kiw.speedy.wiring.StubWiring;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpeedyMessagingStubTest {

    private final SpeedyHost subscriber1Host = new SpeedyHost("subscriber", "127.0.0.1", 9000);
    private final SpeedyHost subscriber2Host = new SpeedyHost("subscriber2", "127.0.0.1", 9001);
    private final SpeedyHost partialSubscriberHost = new SpeedyHost("partialSubscriber", "127.0.0.1", 9003);
    private final SpeedyHost publisherHost = new SpeedyHost("publisher", "127.0.0.1", 9010);
    private final SpeedyHost publisher2Host = new SpeedyHost("publisher2", "127.0.0.1", 9011);
    private List<String> seenMessages = new ArrayList<>();
    private List<String> subASeenMessages = new ArrayList<>();
    private List<String> seenMessages2 = new ArrayList<>();
    private List<String> subscriber2SeenMessages = new ArrayList<>();
    private List<String> partialSeenMessages = new ArrayList<>();
    private SpeedyMessagingImpl subscriber;
    private SpeedyMessagingImpl subscriber2;
    private SpeedyMessagingImpl publisher;
    private SpeedyMessagingImpl publisher2;
    private final AtomicBoolean hasErrored = new AtomicBoolean(false);
    private StubRouter stubRouter;
    private SpeedyMessagingImpl partialSubscriber;
    private SpeedyMessagingImpl puba;
    private SpeedyMessagingImpl suba;
    private StubWiring publisherWiring;

    @Before
    public void setup() throws Exception {
        stubRouter = new StubRouter();
        Collection<String> messageKeys = Arrays.asList("message", "message2");

        SpeedyNetwork speedyNetwork = new SpeedyNetworkBuilder().defaultWindowSize(16)
                .addChannel(SpeedyChannel
                        .newChannel("messages")
                        .withPublishers(publisherHost, publisher2Host)
                        .forKeys("message", "message2")
                        .toSubscribers(subscriber1Host))

                .addChannel(SpeedyChannel
                        .newChannel("messagesSubscriber2")
                        .withPublishers(publisherHost)
                        .forKeys("message", "message2")
                        .toSubscribers(subscriber2Host))

                .addChannel(SpeedyChannel
                        .newChannel("error")
                        .withPublishers(publisherHost)
                        .forKeys("error")
                        .toSubscribers(subscriber1Host))


                .addChannel(SpeedyChannel
                        .newChannel("partial")
                        .withPublishers(publisherHost)
                        .forKeys("message")
                        .toSubscribers(partialSubscriberHost))

                .buildNetwork();
        subscriber = speedyNetwork.createInstanceBuilderFromPerspective(subscriber1Host)
                .withOnMessageErrorHandler(t -> hasErrored.set(true))
                .withWiring(new StubWiring(stubRouter, subscriber1Host))
                .build();

        subscriber2 = speedyNetwork.createInstanceBuilderFromPerspective(subscriber2Host)
                .withWiring(new StubWiring(stubRouter, subscriber2Host))
                .build();

        partialSubscriber = speedyNetwork.createInstanceBuilderFromPerspective(partialSubscriberHost)
                .withWiring(new StubWiring(stubRouter, partialSubscriberHost))
                .build();


        publisherWiring = new StubWiring(stubRouter, publisherHost);
        publisher = speedyNetwork.createInstanceBuilderFromPerspective(publisherHost)
                .withWiring(publisherWiring)
                .build();

        publisher2 = speedyNetwork.createInstanceBuilderFromPerspective(publisher2Host)
                .withWiring(new StubWiring(stubRouter, publisher2Host))
                .build();

        SpeedyHost pubaHost = new SpeedyHost("puba", "1.1.1.1", 10001);
        SpeedyHost subaHost = new SpeedyHost("suba", "1.1.1.2", 10002);

        SpeedyNetwork isolatedNetwork = new SpeedyNetworkBuilder().defaultWindowSize(16)
                .addChannel(SpeedyChannel
                        .newChannel("p2p")
                        .withPublishers(pubaHost).forKeys("p2pmessage").toSubscribers(subaHost))
                .buildNetwork();

        puba = isolatedNetwork.createInstanceBuilderFromPerspective(pubaHost).withWiring(new StubWiring(stubRouter, pubaHost)).build();
        suba = isolatedNetwork.createInstanceBuilderFromPerspective(subaHost).withWiring(new StubWiring(stubRouter, subaHost)).build();
        suba.subscribe("p2pmessage",message ->  subASeenMessages.add(new String(ByteBufferHelper.toByteArray(message))));
        subscriber.subscribe("message", message -> seenMessages.add(new String(ByteBufferHelper.toByteArray(message))));

        subscriber.subscribe("error", message -> {
            throw new RuntimeException("death");
        });

        partialSubscriber.subscribe("message", message -> partialSeenMessages.add(new String(ByteBufferHelper.toByteArray(message))));

        subscriber.subscribe("message2", message -> {
            seenMessages2.add(new String(ByteBufferHelper.toByteArray(message)));
        });

        subscriber2.subscribe("message", message -> {
            subscriber2SeenMessages.add(new String(ByteBufferHelper.toByteArray(message)));
        });

        subscriber2.subscribe("message2", message -> {
        });

        puba.start();
        suba.start();
        publisher.start();
        publisher2.start();
        subscriber.start();
        subscriber2.start();
        partialSubscriber.start();
        stubRouter.start();
    }

    @Test
    public void shouldSendMessageToSubscriber() throws Exception {
        String message = "hello this is a message containing much stuff";
        publisher.publish("message", asByteBuffer(message.getBytes()));

        assertEquals(message, seenMessages.get(0));
    }

    @Test
    public void shouldSendMessageToSubscribersInDifferentBucketsSharingAKey() throws Exception {
        String message = "hello this is a message containing much stuff1";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        publisher.publish("message", asByteBuffer(message.getBytes()));
        publisher.publish("message2", asByteBuffer(message2.getBytes()));
        publisher.publish("message", asByteBuffer(message3.getBytes()));

        assertEquals(message, seenMessages.get(0));
        assertEquals(message3, seenMessages.get(1));

        assertEquals(message2, seenMessages2.get(0));

        assertEquals(message, partialSeenMessages.get(0));
        assertEquals(message3, partialSeenMessages.get(1));
    }

    @Test
    public void shouldSendMessagesToSubscriberForDifferentKeys() throws Exception {
        String message = "hello this is a message containing much stuff";
        String message2 = "hello this is a message2 containing much stuff";
        publisher.publish("message", asByteBuffer(message.getBytes()));
        publisher.publish("message2", asByteBuffer(message2.getBytes()));
        publisher.publish("message", asByteBuffer(message2.getBytes()));
        publisher.publish("message2", asByteBuffer(message.getBytes()));

        assertEquals(message, seenMessages.get(0));
        assertEquals(message2, seenMessages.get(1));
        assertEquals(message2, seenMessages2.get(0));
        assertEquals(message, seenMessages2.get(1));
    }

    @Test
    public void shouldUseSubscriberErrorHandlerOnThrowInSSubscription() throws Exception {
        publisher.publish("error", asByteBuffer("something else".getBytes()));

        assertTrue(hasErrored.get());
    }

    @Test
    public void shouldRecoverALostMessage() throws Exception {


        String message = "hello this is a message containing much stuff1";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        puba.publish("p2pmessage", asByteBuffer(message.getBytes()));
        stubRouter.startDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message2.getBytes()));
        stubRouter.stopDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message3.getBytes()));

        stubRouter.fastForwardTimeInNanos(1_000_000_000);
        stubRouter.onPulse();

        assertEquals(message, subASeenMessages.get(0));
        assertEquals(message2, subASeenMessages.get(1));
        assertEquals(message3, subASeenMessages.get(2));
    }


    @Test
    public void shouldRecoverRangeOfLostMessages() throws Exception {


        String message = "hello this is a message containing much stuff1";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        String message4 = "hello this is a message containing much stuff4";
        puba.publish("p2pmessage", asByteBuffer(message.getBytes()));
        stubRouter.startDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message2.getBytes()));
        puba.publish("p2pmessage", asByteBuffer(message3.getBytes()));
        stubRouter.stopDroppingPackets();


        puba.publish("p2pmessage", asByteBuffer(message4.getBytes()));

        stubRouter.fastForwardTimeInNanos(1_000_000_000);
        stubRouter.onPulse();

        assertEquals(message, subASeenMessages.get(0));
        assertEquals(message2, subASeenMessages.get(1));
        assertEquals(message3, subASeenMessages.get(2));
        assertEquals(message4, subASeenMessages.get(3));
    }


    @Test
    public void shouldRecoverIntervalsofLostMessagesOnlyAfterAReasonableTimePeriodHasPassed() throws Exception {


        String message = "hello this is a message containing much stuff1";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        String message4 = "hello this is a message containing much stuff4";
        String message5 = "hello this is a message containing much stuff5";
        puba.publish("p2pmessage", asByteBuffer(message.getBytes()));
        stubRouter.startDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message2.getBytes()));
        stubRouter.stopDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message3.getBytes()));


        stubRouter.startDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message4.getBytes()));
        stubRouter.stopDroppingPackets();

        stubRouter.fastForwardTimeInNanos(100_000_001);
        stubRouter.onPulse();
        puba.publish("p2pmessage", asByteBuffer(message5.getBytes()));

        stubRouter.fastForwardTimeInNanos(1_000_000_000);
        stubRouter.onPulse();
        assertEquals(message, subASeenMessages.get(0));
        assertEquals(message2, subASeenMessages.get(1));
        assertEquals(message3, subASeenMessages.get(2));
        assertEquals(message4, subASeenMessages.get(3));
        assertEquals(message5, subASeenMessages.get(4));
    }

    @Test
    public void shouldNotRecoverIntervalsofLostMessagesUntilNackResendDelayHasExpired() throws Exception {


        String message = "hello this is a message containing much stuff1";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        String message4 = "hello this is a message containing much stuff4";
        String message5 = "hello this is a message containing much stuff5";
        String message6 = "hello this is a message containing much stuff6";
        puba.publish("p2pmessage", asByteBuffer(message.getBytes()));
        stubRouter.startDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message2.getBytes()));
        stubRouter.stopDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message3.getBytes()));


        stubRouter.startDroppingPackets();
        puba.publish("p2pmessage", asByteBuffer(message4.getBytes()));
        stubRouter.stopDroppingPackets();

        stubRouter.fastForwardTimeInNanos(100_000_000);
        stubRouter.onPulse();
        puba.publish("p2pmessage", asByteBuffer(message5.getBytes()));

        assertEquals(3, subASeenMessages.size());
        assertEquals(message, subASeenMessages.get(0));
        assertEquals(message2, subASeenMessages.get(1));
        assertEquals(message3, subASeenMessages.get(2));

        stubRouter.fastForwardTimeInNanos(1);
        puba.publish("p2pmessage", asByteBuffer(message6.getBytes()));

        stubRouter.fastForwardTimeInNanos(1_000_000_000);
        stubRouter.onPulse();
        assertEquals(message4, subASeenMessages.get(3));
        assertEquals(message5, subASeenMessages.get(4));
        assertEquals(message6, subASeenMessages.get(5));
    }

    @Test
    public void shouldHandleMessageGreaterThanMessageSize() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 16000; i++) {
            stringBuilder.append("String" + i);
        }
        String message = stringBuilder.toString();
        assertTrue(message.getBytes().length > SpeedyMessagingImpl.DATAGRAM_LENGTH * 3);

        publisher.publish("message", asByteBuffer(message.getBytes()));

        assertEquals(message, seenMessages.get(0));
    }

    @Test
    public void shouldMaintainOrder() throws Exception {

        int messagesToSend = 10000;
        for (int i = 0; i < messagesToSend; i++) {
            publisher.publish("message", asByteBuffer(("stuff" + i).getBytes()));
        }

        assertEquals("stuff0", seenMessages.get(0));
        assertEquals(messagesToSend, seenMessages.size());

        for (int i = 0; i < messagesToSend; i++) {
            assertEquals("stuff" + i, seenMessages.get(i));
        }
    }

    @Test
    public void shouldHandleMultiplePublishersOfTheSameKeyForSmallMessages() throws Exception {


        int iterationsPerPublisher = 50000;
        ArrayList<String> expectedPublisher1Messages = new ArrayList<>();
        ArrayList<String> expectedPublisher2Messages = new ArrayList<>();
        int messagesToSend = iterationsPerPublisher;
        for (int i = 0; i < messagesToSend; i++) {
            String message = "1pub" + i;
            String message2 = "2pub" + i;

            expectedPublisher1Messages.add(message);
            expectedPublisher2Messages.add(message2);
            publisher.publish("message", asByteBuffer(message.getBytes()));
            publisher2.publish("message", asByteBuffer(message2.getBytes()));

        }

        assertEquals(iterationsPerPublisher * 2, seenMessages.size());
        List<String> actualPublisher1Messages = seenMessages.stream().filter(sm -> sm.startsWith("1pub")).collect(Collectors.toList());
        assertEquals(iterationsPerPublisher, actualPublisher1Messages.size());
        for (int i = 0; i < iterationsPerPublisher; i++) {
            assertEquals(expectedPublisher1Messages.get(i), actualPublisher1Messages.get(i));
        }

        List<String> actualPublisher2Messages = seenMessages.stream().filter(sm -> sm.startsWith("2pub")).collect(Collectors.toList());
        assertEquals(iterationsPerPublisher, actualPublisher2Messages.size());
        for (int i = 0; i < iterationsPerPublisher; i++) {
            assertEquals(expectedPublisher2Messages.get(i), actualPublisher2Messages.get(i));
        }
    }

    @Test
    public void shouldHandleMultiplePublishersOfTheDifferentKeysForSmallMessages() throws Exception {


        int iterationsPerPublisher = 50000;
        ArrayList<String> expectedPublisher1Messages = new ArrayList<>();
        ArrayList<String> expectedPublisher2Messages = new ArrayList<>();
        int messagesToSend = iterationsPerPublisher;
        for (int i = 0; i < messagesToSend; i++) {
            String message = "1pub" + i;
            String message2 = "2pub" + i;

            expectedPublisher1Messages.add(message);
            expectedPublisher2Messages.add(message2);

            publisher.publish("message", asByteBuffer(message.getBytes()));
            publisher2.publish("message2", asByteBuffer(message2.getBytes()));

        }

        assertEquals(iterationsPerPublisher, seenMessages.size());
        for (int i = 0; i < iterationsPerPublisher; i++) {
            assertEquals(expectedPublisher1Messages.get(i), seenMessages.get(i));
        }

        assertEquals(iterationsPerPublisher, seenMessages2.size());
        for (int i = 0; i < iterationsPerPublisher; i++) {
            assertEquals(expectedPublisher2Messages.get(i), seenMessages2.get(i));
        }
    }


    @Test
    public void shouldHandleMultipleSubscribersForTheSameKey() throws Exception {

        String message = "hello this is a message containing much stuff";
        publisher.publish("message", asByteBuffer(message.getBytes()));

        assertEquals(message, seenMessages.get(0));
        assertEquals(message, subscriber2SeenMessages.get(0));
    }

    @Test
    public void shouldHandleMessageGreaterThanMessageSizeToMultileSubscribers() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 16000; i++) {
            stringBuilder.append("String" + i);
        }
        String message = stringBuilder.toString();
        assertTrue(message.getBytes().length > SpeedyMessagingImpl.DATAGRAM_LENGTH * 3);

        publisher.publish("message", asByteBuffer(message.getBytes()));

        assertEquals(message, seenMessages.get(0));
        assertEquals(message, subscriber2SeenMessages.get(0));
    }

    @Test
    public void shouldSendMultipleMessagesInASinglePacket() throws Exception {

        String message = "hello this is a message containing much stuff";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        publisherWiring.setAutoFlushing(false);
        publisher.publish("message", asByteBuffer(message.getBytes()));
        publisher.publish("message", asByteBuffer(message2.getBytes()));

        assertTrue(seenMessages.size() == 0);
        publisherWiring.setAutoFlushing(true);
        publisher.publish("message", asByteBuffer(message3.getBytes()));

        assertEquals(message, seenMessages.get(0));
        assertEquals(message2, seenMessages.get(1));
        assertEquals(message3, seenMessages.get(2));
    }

    @Test
    public void shouldNotFlushIfMessageIsNotLargeEnoughThatAnotherCouldNotFitInThePacket() throws Exception {

        byte[] bytes = new byte[1350];
        publisherWiring.setAutoFlushing(false);
        publisher.publish("message", asByteBuffer(bytes));

        assertEquals(0, seenMessages.size());
    }

    private static ByteBuffer asByteBuffer(byte[] bytes) {
        ByteBuffer allocate = ByteBuffer.allocateDirect(bytes.length);
        allocate.put(bytes);
        allocate.flip();
        return allocate;
    }

}