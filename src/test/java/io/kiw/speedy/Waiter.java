package io.kiw.speedy;

public class Waiter {
    public static void waitFor(Runnable taskToBeTrue) throws InterruptedException {
        int attempt = 0;
        while(true)
        {
            attempt++;
            try
            {
                taskToBeTrue.run();
                break;
            }
            catch (Throwable e)
            {
                if(attempt == 10)
                {
                    throw e;
                }
                Thread.sleep(1000);
            }
        }
    }
}
