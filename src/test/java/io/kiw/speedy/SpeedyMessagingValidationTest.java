package io.kiw.speedy;

import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;
import io.kiw.speedy.exception.ConflictingRouteException;
import io.kiw.speedy.exception.InvalidSubscriptionException;
import io.kiw.speedy.exception.SpeedyMessagingInitiationException;
import io.kiw.speedy.exception.SpeedyMessagingNotInitiatedException;
import org.junit.Test;

import java.nio.ByteBuffer;

public class SpeedyMessagingValidationTest {

    private SpeedyHost bob = new SpeedyHost("bob", "127.0.0.1", 20022);
    private SpeedyHost alice = new SpeedyHost("alice", "127.0.0.1", 20023);
    private SpeedyHost alice2 = new SpeedyHost("alice2", "127.0.0.1", 20024);
    private SpeedyHost alice3 = new SpeedyHost("alice3", "127.0.0.1", 20025);

    @Test(expected = InvalidSubscriptionException.class)
    public void shouldErrorIfSubscribingToAnUnRegisteredKey() throws Exception {
        SpeedyMessagingImpl bobInstance = new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel.newChannel("aKeyChannel").withPublishers(bob).forKeys("akey").toSubscribers(alice))
                .buildNetwork()
                .createInstanceBuilderFromPerspective(bob).build();
        bobInstance.subscribe("somethingElse", m -> {});
    }

    @Test
    public void shouldAllowSubscribingToRegisteredKey() throws Exception {
        SpeedyMessagingImpl bobInstance = new SpeedyNetworkBuilder()

                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(alice)
                        .forKeys("aKey")
                        .toSubscribers(bob))
                .buildNetwork()
                .createInstanceBuilderFromPerspective(bob).build();
        bobInstance.subscribe("aKey", m -> {});
    }

    @Test(expected = SpeedyMessagingInitiationException.class)
    public void shouldErrorIfStartingWithoutSubscribingToAllRegisteredKeys() throws Exception {
        SpeedyMessagingImpl bobInstance = new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(alice)
                        .forKeys("akey", "aKey2")
                        .toSubscribers(bob))

                .buildNetwork()
                .createInstanceBuilderFromPerspective(bob).build();
        bobInstance.subscribe("aKey2", m -> {

        });

        bobInstance.start();
    }

    @Test(expected = SpeedyMessagingNotInitiatedException.class)
    public void shouldErrorIfPublishingToUnregisteredKey() throws Exception {
        SpeedyMessagingImpl bobInstance = new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                .newChannel("aKeyChannel")
                .withPublishers(bob)
                .forKeys("akey2")
                .toSubscribers(alice))

                .buildNetwork()
                .createInstanceBuilderFromPerspective(bob).build();

        bobInstance.publish("aKey2", asByteBuffer("whatever".getBytes()));
    }


    @Test(expected = ConflictingRouteException.class)
    public void shouldErrorWhenRegisteringSubscriptionRouteWithDuplicatePublisherAndKeyPair() throws Exception
    {
        new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(alice)
                        .forKeys("akey", "aKey2", "aKey3")
                        .toSubscribers(bob))

                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel2")
                        .withPublishers(alice)
                        .forKeys( "aKey2")
                        .toSubscribers(bob))

                .buildNetwork().createInstanceBuilderFromPerspective(bob);
    }

    @Test
    public void shouldAllowRegisteringSubscriptionRouteWithDuplicatePublisherButWithDifferentKey() throws Exception {
        new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(alice)
                        .forKeys("akey", "aKey2", "aKey3")
                        .toSubscribers(bob))

                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel2")
                        .withPublishers(alice)
                        .forKeys( "somethingElse")
                        .toSubscribers(bob))
                .buildNetwork().createInstanceBuilderFromPerspective(bob).build();

    }


    @Test
    public void shouldAllowRegisteringSubscriptionRouteWithDuplicateKeyButWithADifferentPublisher() throws Exception {
        new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(alice)
                        .forKeys("akey", "aKey2", "aKey3")
                        .toSubscribers(bob))

                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel2")
                        .withPublishers(alice2)
                        .forKeys( "aKey2")
                        .toSubscribers(bob))
                .buildNetwork().createInstanceBuilderFromPerspective(bob).build();

    }


    @Test(expected = ConflictingRouteException.class)
    public void shouldErrorWhenRegisteringPublisherRouteWithDuplicateSubscriberAndKeyPair() throws Exception {
        new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(alice)
                        .forKeys("akey", "aKey2", "aKey3")
                        .toSubscribers(bob))

                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel2")
                        .withPublishers(alice)
                        .forKeys( "aKey2")
                        .toSubscribers(bob))

                .buildNetwork().createInstanceBuilderFromPerspective(bob);

    }


    @Test
    public void shouldAllowRegisteringPublisherRouteWithDuplicateKeyButWithADifferentSubscriber() throws Exception {
        new SpeedyNetworkBuilder()
                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel")
                        .withPublishers(bob)
                        .forKeys("akey", "aKey2", "aKey3")
                        .toSubscribers(alice))

                .addChannel(SpeedyChannel
                        .newChannel("aKeyChannel2")
                        .withPublishers(bob)
                        .forKeys( "aKey2")
                        .toSubscribers(alice2))
                .buildNetwork()
                .createInstanceBuilderFromPerspective(bob);


    }

    private static ByteBuffer asByteBuffer(byte[] bytes) {
        ByteBuffer allocate = ByteBuffer.allocateDirect(bytes.length);
        allocate.put(bytes);
        allocate.flip();
        return allocate;
    }
}