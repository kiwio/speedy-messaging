package io.kiw.speedy;

import io.kiw.speedy.builder.ChannelOptions;
import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetwork;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;
import io.kiw.speedy.helper.ByteBufferHelper;
import io.kiw.speedy.wiring.LinuxUdpSpeedyWiring;
import org.junit.After;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static io.kiw.speedy.Waiter.waitFor;
import static io.kiw.speedy.parallel.ParallelHelper.parallel;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SpeedyMessagingNativeTest {

    private static final int WINDOW_SIZE = 64;
    private final SpeedyHost subscriber1Host = new SpeedyHost("subscriber", "127.0.0.1", 9000);
    private final SpeedyHost subscriber2Host = new SpeedyHost("subscriber2", "127.0.0.1", 9001);
    private final SpeedyHost partialSubscriberHost = new SpeedyHost("partialSubscriber", "127.0.0.1", 9003);
    private final SpeedyHost publisherHost = new SpeedyHost("publisher", "127.0.0.1", 9010);
    private final SpeedyHost publisher2Host = new SpeedyHost("publisher2", "127.0.0.1", 9011);
    private List<String> seenMessages = new ArrayList<>();
    private List<String> seenMessages2 = new ArrayList<>();
    private List<String> subscriber2SeenMessages = new ArrayList<>();
    private List<String> partialSeenMessages = new ArrayList<>();
    private List<String> multiconsumerSeenMessages = Collections.synchronizedList(new ArrayList<>());
    private SpeedyMessagingImpl subscriber;
    private SpeedyMessagingImpl subscriber2;
    private SpeedyMessagingImpl publisher;
    private SpeedyMessagingImpl publisher2;
    private final AtomicBoolean hasErrored = new AtomicBoolean(false);
    private SpeedyMessagingImpl partialSubscriber;
    private boolean ranTest = false;

    @Before
    public void setup() throws Exception {

        try
        {
            System.loadLibrary("linuxdatachannel");
        }
        catch (UnsatisfiedLinkError e)
        {
            Assume.assumeTrue(false);
        }

        ranTest = true;

        SpeedyNetwork speedyNetwork = new SpeedyNetworkBuilder()
                .defaultWindowSize(WINDOW_SIZE)
                .addChannel(SpeedyChannel
                        .newChannel("messages")
                        .withPublishers(publisherHost, publisher2Host)
                        .forKeys("message", "message2")
                        .toSubscribers(subscriber1Host, subscriber2Host))
                .addChannel(SpeedyChannel
                        .newChannel("error")
                        .withPublishers(publisherHost, publisher2Host)
                        .forKeys("error")
                        .toSubscribers(subscriber1Host))
                .addChannel(SpeedyChannel
                        .newChannel("singleMessage")
                        .withPublishers(publisherHost)
                        .forKeys("message")
                        .toSubscribers(partialSubscriberHost))
                .addChannel(SpeedyChannel
                .newChannel("multiConsumer")
                .withPublishers(publisherHost)
                .forKeys("multiKey")
                .toSubscribers(subscriber1Host)
                .withOptions(ChannelOptions.defaultOptions().withSubscriberThreads(8)))
                .buildNetwork();

        subscriber = speedyNetwork.createInstanceBuilderFromPerspective(subscriber1Host)
                .withWiring(new LinuxUdpSpeedyWiring(subscriber1Host.getPort()))
                .withOnMessageErrorHandler(t -> hasErrored.set(true))
                .build();

        subscriber2 = speedyNetwork.createInstanceBuilderFromPerspective(subscriber2Host)
                .withWiring(new LinuxUdpSpeedyWiring(subscriber2Host.getPort()))
                .build();
        partialSubscriber = speedyNetwork.createInstanceBuilderFromPerspective(partialSubscriberHost)
                .withWiring(new LinuxUdpSpeedyWiring(partialSubscriberHost.getPort()))
                .build();
        publisher = speedyNetwork.createInstanceBuilderFromPerspective(publisherHost)
                .withWiring(new LinuxUdpSpeedyWiring(publisherHost.getPort()))
                .build();
        publisher2 = speedyNetwork.createInstanceBuilderFromPerspective(publisher2Host)
                .withWiring(new LinuxUdpSpeedyWiring(publisher2Host.getPort()))
                .build();

        subscriber.subscribe("message", message -> seenMessages.add(new String(ByteBufferHelper.toByteArray(message))));

        subscriber.subscribe("error", message -> {
            throw new RuntimeException("death");
        });

        subscriber.subscribe("message2", message -> seenMessages2.add(new String(ByteBufferHelper.toByteArray(message))));
        subscriber.subscribe("multiKey", message -> multiconsumerSeenMessages.add(new String(ByteBufferHelper.toByteArray(message))));
        subscriber2.subscribe("message", message -> subscriber2SeenMessages.add(new String(ByteBufferHelper.toByteArray(message))));
        subscriber2.subscribe("message2", m -> {});
        partialSubscriber.subscribe("message", message -> partialSeenMessages.add(new String(ByteBufferHelper.toByteArray(message))));

        parallel(
            () -> subscriber.start(),
            () -> subscriber2.start(),
            () -> publisher.start(),
            () -> publisher2.start(),
            () -> partialSubscriber.start()
        );
    }

    @Test
    public void shouldSendMessageToSubscriber() throws Exception {
        String message = "hello this is a message containing much stuff";
        publisher.publish("message", asByteBuffer(message.getBytes()));

        waitFor(() -> assertEquals(message, seenMessages.get(0)));
    }

    @Test
    public void shouldSendMessageToMultiThreadSubscriber() throws Exception {
        String message = "hello this is a message containing much stuff";

        for (int i = 0; i < 5; i++) {
            publisher.publish("multiKey", asByteBuffer((message + i).getBytes()));
        }


        for (int i = 0; i < 5; i++) {
            String expectedMessage = message + i;
            waitFor(() -> {
                assertTrue("Expected To find message containing '" + expectedMessage + "' but got '" + message + "'", multiconsumerSeenMessages.contains(expectedMessage));
            });
        }
    }

    @Test
    public void shouldUseSubscriberErrorHandlerOnThrowInSSubscription() throws Exception {
        publisher.publish("error", asByteBuffer("something else".getBytes()));

        waitFor(() -> assertTrue(hasErrored.get()));
    }

    @Test
    public void shouldSendMessageToSubscribersInDifferentBucketsSharingAKey() throws Exception {
        String message = "hello this is a message containing much stuff1";
        String message2 = "hello this is a message containing much stuff2";
        String message3 = "hello this is a message containing much stuff3";
        publisher.publish("message", asByteBuffer(message.getBytes()));
        publisher.publish("message2", asByteBuffer(message2.getBytes()));
        publisher.publish("message", asByteBuffer(message3.getBytes()));

        waitFor(() -> {
            assertEquals(message, seenMessages.get(0));
            assertEquals(message3, seenMessages.get(1));

            assertEquals(message2, seenMessages2.get(0));

            assertEquals(message, partialSeenMessages.get(0));
            assertEquals(message3, partialSeenMessages.get(1));
        });
    }

    @Test
    public void shouldHandleMessageGreaterThanMessageSize() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 16000; i++) {
            stringBuilder.append("String").append(i);
        }
        String message = stringBuilder.toString();
        assertTrue(message.getBytes().length > SpeedyMessagingImpl.DATAGRAM_LENGTH * 3);

        publisher.publish("message", asByteBuffer(message.getBytes()));

        waitFor(() -> assertEquals(message, seenMessages.get(0)));
    }

    @Test
    public void shouldMaintainOrder() throws Exception {

        int messagesToSend = 10000;
        for (int i = 0; i < messagesToSend; i++) {
            publisher.publish("message", asByteBuffer(("stuff" + i).getBytes()));
        }

        waitFor(() -> {
            assertEquals("stuff0", seenMessages.get(0));
            assertEquals(messagesToSend, seenMessages.size());
        });

        waitFor(() -> {
            for (int i = 0; i < messagesToSend; i++) {
                assertEquals("stuff" + i, seenMessages.get(i));
            }
        });
    }

    @Test
    public void shouldHandleMultiplePublishersOfTheSameKeyForSmallMessages() throws Exception
    {

        int iterationsPerPublisher = 50000;
        ArrayList<String> expectedPublisher1Messages = new ArrayList<>();
        ArrayList<String> expectedPublisher2Messages = new ArrayList<>();
        parallel(() -> {
            for (int i = 0; i < iterationsPerPublisher; i++) {
                String message = "1pub" + i;
                expectedPublisher1Messages.add(message);
                publisher.publish("message", asByteBuffer(message.getBytes()));
            }
        }, () -> {
            for (int i = 0; i < iterationsPerPublisher; i++) {
                String message = "2pub" + i;
                expectedPublisher2Messages.add(message);
                publisher2.publish("message", asByteBuffer(message.getBytes()));
            }
        });

        waitFor(() -> {
            assertEquals(iterationsPerPublisher * 2, seenMessages.size());
            List<String> actualPublisher1Messages = seenMessages.stream().filter(sm -> sm.startsWith("1pub")).collect(Collectors.toList());
            assertEquals(iterationsPerPublisher, actualPublisher1Messages.size());
            for (int i = 0; i < iterationsPerPublisher; i++) {
                assertEquals(expectedPublisher1Messages.get(i), actualPublisher1Messages.get(i));
            }

            List<String> actualPublisher2Messages = seenMessages.stream().filter(sm -> sm.startsWith("2pub")).collect(Collectors.toList());
            assertEquals(iterationsPerPublisher, actualPublisher2Messages.size());
            for (int i = 0; i < iterationsPerPublisher; i++) {
                assertEquals(expectedPublisher2Messages.get(i), actualPublisher2Messages.get(i));
            }
        });


    }

    @Test
    public void shouldHandleMultiplePublishersOfTheDifferentKeysForSmallMessages() throws Exception {

        int iterationsPerPublisher = 10000;
        ArrayList<String> expectedPublisher1Messages = new ArrayList<>();
        ArrayList<String> expectedPublisher2Messages = new ArrayList<>();
        parallel(() -> {
            for (int i = 0; i < iterationsPerPublisher; i++) {
                String message = "1pub" + i;
                expectedPublisher1Messages.add(message);
                publisher.publish("message", asByteBuffer(message.getBytes()));
            }
        }, () -> {
            for (int i = 0; i < iterationsPerPublisher; i++) {
                String message = "2pub" + i;
                expectedPublisher2Messages.add(message);
                publisher2.publish("message2", asByteBuffer(message.getBytes()));
            }
        });

        waitFor(() -> {
            assertEquals(iterationsPerPublisher, seenMessages.size());
            for (int i = 0; i < iterationsPerPublisher; i++) {
                assertEquals(expectedPublisher1Messages.get(i), seenMessages.get(i));
            }

            assertEquals(iterationsPerPublisher, seenMessages2.size());
            for (int i = 0; i < iterationsPerPublisher; i++) {
                assertEquals(expectedPublisher2Messages.get(i), seenMessages2.get(i));
            }
        });
    }

    @Test
    public void shouldHandleMultiplePublishersOfTheSameKeyForBigMessages() throws Exception {


        int iterationsPerPublisher = 500;
        ArrayList<String> expectedPublisher1Messages = new ArrayList<>();
        ArrayList<String> expectedPublisher2Messages = new ArrayList<>();
        parallel(() -> {
            for (int i = 0; i < iterationsPerPublisher; i++) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < 1000; j++) {
                    stringBuilder.append("1pub").append(j);
                }
                String message = stringBuilder.toString();
                expectedPublisher1Messages.add(message);
                publisher.publish("message", asByteBuffer(message.getBytes()));
            }
        }, () -> {
            for (int i = 0; i < iterationsPerPublisher; i++) {
                StringBuilder stringBuilder = new StringBuilder();
                for (int j = 0; j < 1000; j++) {
                    stringBuilder.append("2pub").append(j);
                }
                String message = stringBuilder.toString();
                expectedPublisher2Messages.add(message);
                publisher2.publish("message2", asByteBuffer(message.getBytes()));
            }
        });

        waitFor(() -> {
            assertEquals(iterationsPerPublisher, seenMessages.size());
            for (int i = 0; i < iterationsPerPublisher; i++) {
                assertEquals(expectedPublisher1Messages.get(i), seenMessages.get(i));
            }

            assertEquals(iterationsPerPublisher, seenMessages2.size());
            for (int i = 0; i < iterationsPerPublisher; i++) {
                assertEquals(expectedPublisher2Messages.get(i), seenMessages2.get(i));
            }
        });
    }

    @Test
    public void shouldHandleMultipleSubscribersForTheSameKey() throws Exception {

        String message = "hello this is a message containing much stuff";
        publisher.publish("message", asByteBuffer(message.getBytes()));

        waitFor(() -> {
            assertEquals(message, seenMessages.get(0));
            assertEquals(message, subscriber2SeenMessages.get(0));
        });
    }

    @Test
    public void shouldHandleMessageGreaterThanMessageSizeToMultileSubscribers() throws Exception {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < 16000; i++) {
            stringBuilder.append("String").append(i);
        }
        String message = stringBuilder.toString();
        assertTrue(message.getBytes().length > SpeedyMessagingImpl.DATAGRAM_LENGTH * 3);

        publisher.publish("message", asByteBuffer(message.getBytes()));

        waitFor(() -> {
            assertEquals(message, seenMessages.get(0));
            assertEquals(message, subscriber2SeenMessages.get(0));
        });
    }

    @After
    public void tearDown() throws Exception {
        if(ranTest)
        {
            subscriber.close();
            subscriber2.close();
            publisher2.close();
            publisher.close();
            partialSubscriber.close();
            Thread.sleep(1000);
        }
    }

    private static ByteBuffer asByteBuffer(byte[] bytes) {
        ByteBuffer allocate = ByteBuffer.allocateDirect(bytes.length);
        allocate.put(bytes);
        allocate.flip();
        return allocate;
    }
}