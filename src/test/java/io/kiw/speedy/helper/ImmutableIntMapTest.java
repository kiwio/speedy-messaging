package io.kiw.speedy.helper;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static java.lang.Math.abs;
import static org.junit.Assert.assertEquals;

public class ImmutableIntMapTest {

    private final Random random = new Random(83726874L);
    @Test
    public void shouldGetFromMap() throws Exception {
        HashMap<Integer, Integer> intMap = new HashMap<>();

        for (int i = 0; i < 200; i++) {
            intMap.put(abs(random.nextInt()), random.nextInt());
        }

        ImmutableIntMap<Integer> integerImmutableIntMap = ImmutableMapFactory.initialiseIntMap(intMap);

        for (Map.Entry<Integer, Integer> integerIntegerEntry : intMap.entrySet()) {
            assertEquals(integerImmutableIntMap.get(integerIntegerEntry.getKey()), integerIntegerEntry.getValue());
        }


    }
}