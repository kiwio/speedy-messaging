package io.kiw.speedy.helper;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import static java.lang.Math.abs;
import static org.junit.Assert.assertEquals;

public class ImmutableLongMapTest {

    private final Random random = new Random();
    @Test
    public void shouldGetFromMap() throws Exception {
        HashMap<Long, Integer> longMap = new HashMap<>();

        for (int i = 0; i < 1000; i++) {
            longMap.put(abs(random.nextLong()), random.nextInt());
        }

        ImmutableLongMap<Integer> immutableLongMap = ImmutableMapFactory.initialiseLongMap(longMap);

        for (Map.Entry<Long, Integer> longIntEntry : longMap.entrySet()) {
            assertEquals(immutableLongMap.get(longIntEntry.getKey()), longIntEntry.getValue());
        }


    }
}