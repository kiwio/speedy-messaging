package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.wiring.linuxnative.NativeDatagramChannel;
import org.junit.Assume;
import org.junit.Before;
import org.junit.Test;

import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static org.junit.Assert.assertEquals;

public class NativeDatagramChannelTest {


    @Before
    public void setUp() throws Exception {
        try
        {
            System.loadLibrary("linuxdatachannel");
        }
        catch (UnsatisfiedLinkError e)
        {
            Assume.assumeTrue(false);
        }
    }

    @Test
    public void shouldSetUpUdpConnection() throws Exception {


        NativeDatagramChannel sender = NativeDatagramChannel.open(9876);
        NativeDatagramChannel receiver = NativeDatagramChannel.open(9877);

        ByteBuffer senderBuffer = ByteBuffer.allocateDirect(1024).order(ByteOrder.nativeOrder());
        ByteBuffer receiverBuffer = ByteBuffer.allocateDirect(1024).order(ByteOrder.nativeOrder());

        senderBuffer.put((byte)34);
        senderBuffer.flip();
        sender.send(senderBuffer, 9877, SpeedyHost.ipAddressBytesToInt(InetAddress.getByName("127.0.0.1").getAddress()));

        receiver.receive(receiverBuffer);
        receiverBuffer.flip();

        assertEquals(34, receiverBuffer.get());
    }
}