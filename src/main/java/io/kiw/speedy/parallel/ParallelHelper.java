package io.kiw.speedy.parallel;

import java.util.ArrayList;
import java.util.List;

public class ParallelHelper {
    public static void parallel(Runnable... runnables)
    {
        final List<Thread> threads = new ArrayList<>();
        for (Runnable runnable : runnables) {
            Thread e = new Thread(runnable);
            threads.add(e);
            e.start();
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }


    }
}
