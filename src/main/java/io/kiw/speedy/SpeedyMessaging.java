package io.kiw.speedy;

import io.kiw.speedy.subscriber.SpeedyMessageHandler;

import java.io.IOException;
import java.nio.ByteBuffer;

public interface SpeedyMessaging {
    void subscribe(String key, SpeedyMessageHandler messageHandler);

    void publish(String key, ByteBuffer bytes);

    void close() throws IOException;
}
