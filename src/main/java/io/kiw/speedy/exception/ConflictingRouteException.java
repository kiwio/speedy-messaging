package io.kiw.speedy.exception;

public class ConflictingRouteException extends RuntimeException {
    public ConflictingRouteException(String message) {
        super(message);
    }
}
