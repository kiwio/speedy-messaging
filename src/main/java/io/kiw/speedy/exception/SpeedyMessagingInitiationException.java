package io.kiw.speedy.exception;

public class SpeedyMessagingInitiationException extends RuntimeException {
    public SpeedyMessagingInitiationException(String s) {
        super(s);
    }
}
