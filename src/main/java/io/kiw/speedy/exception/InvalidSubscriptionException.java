package io.kiw.speedy.exception;

public class InvalidSubscriptionException extends RuntimeException {
    public InvalidSubscriptionException(final String key) {
        super("Was unable to subscribe to key '" + key + "'" + " as is was not registered");
    }
}
