package io.kiw.speedy.exception;

public class SpeedyMessagingNotInitiatedException extends RuntimeException {
    public SpeedyMessagingNotInitiatedException(final String errorMessage) {
        super(errorMessage);
    }
}
