package io.kiw.speedy.publisher;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.channel.PublisherSequenceState;

import java.nio.ByteBuffer;

public class PacketFlusher {


    private final PacketSender packetSender;

    public PacketFlusher(PacketSender packetSender) {

        this.packetSender = packetSender;
    }

    public void flushNewMessage(final ByteBuffer packetBuffer, final SpeedyConnection[] connectionsToPublishTo, PublisherSequenceState packetSequenceState) {
        for (SpeedyConnection remoteConnection : connectionsToPublishTo) {
            packetBuffer.flip();

            packetSender.sendPacket(packetBuffer, remoteConnection.getHost());
        }

        packetSequenceState.incrementAndMarshallNextPacketHeader();
    }

    public void flushResendMessage(final ByteBuffer packetBuffer, final SpeedyConnection[] connectionsToPublishTo) {
        for (SpeedyConnection remoteConnection : connectionsToPublishTo) {
            packetBuffer.flip();
            packetSender.sendPacket(packetBuffer, remoteConnection.getHost());
        }

    }
}
