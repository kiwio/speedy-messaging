package io.kiw.speedy.publisher;

import io.kiw.speedy.PublisherBucket;

import java.nio.ByteBuffer;

public interface PublishPromise {
    void publish(String key, ByteBuffer data);

    void resend(PublisherBucket sequenceState, long firstPacketKeyMissing, long lastPacketKeyMissing);
}
