package io.kiw.speedy.publisher;

import io.kiw.speedy.SpeedyConnection;

import java.util.Collection;

public class SendMessageRequest {
    private final String key;
    private final byte[] marshalledData;
    private final Collection<SpeedyConnection> connections;

    public SendMessageRequest(String key, byte[] marshalledData, Collection<SpeedyConnection> connections) {
        this.key = key;

        this.marshalledData = marshalledData;
        this.connections = connections;
    }

    public byte[] getMarshalledData() {
        return marshalledData;
    }

    public Collection<SpeedyConnection> getConnections() {
        return connections;
    }

    public String getKey() {
        return key;
    }
}
