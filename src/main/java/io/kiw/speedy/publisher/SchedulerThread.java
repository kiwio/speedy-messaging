package io.kiw.speedy.publisher;


import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.LongConsumer;
import java.util.function.LongSupplier;

import static java.util.concurrent.locks.LockSupport.parkNanos;

public class SchedulerThread {

    public static final int WAIT_TO_CHECK_TIME = 10000;
    private final ScheduledJob[] scheduledJobs = new ScheduledJob[2];
    private final LongSupplier nanoTimeRetriever;
    private boolean started;
    private final Thread schedulerThread = new Thread(() -> {
        while (!Thread.interrupted()) {
            this.pulse();
            parkNanos(WAIT_TO_CHECK_TIME);
        }
    }, "Scheduler-Thread");

    public SchedulerThread(LongSupplier nanoTimeRetriever) {
        this.nanoTimeRetriever = nanoTimeRetriever;
    }

    public void addNackJob(final ScheduledJob scheduledJob)
    {
        addJob(scheduledJob, 1);
    }

    public void addFlushJob(final ScheduledJob scheduledJob)
    {
        addJob(scheduledJob, 0);
    }

    private void addJob(final ScheduledJob scheduledJob, int i)
    {
        if(!started)
        {
            scheduledJobs[i]= scheduledJob;
        }
        else
        {
            throw new RuntimeException("Can't add a new job while already started");
        }
    }
    public void start()
    {
        started = true;
        schedulerThread.start();

//        scheduledExecutorService.scheduleWithFixedDelay(this::pulse,
//                WAIT_TO_CHECK_TIME, WAIT_TO_CHECK_TIME, TimeUnit.NANOSECONDS);
    }

    public void pulse() {
        long nanoTime = nanoTimeRetriever.getAsLong();
        scheduledJobs[0].run(nanoTime);
        scheduledJobs[1].run(nanoTime);
    }

    public void stop() {
        schedulerThread.interrupt();
    }

    public static class ScheduledJob {
        private final long interval;
        private long lastRunTime;
        private final LongConsumer consumer;

        public ScheduledJob(long interval, long lastRunTime, LongConsumer consumer) {

            this.interval = interval;
            this.lastRunTime = lastRunTime;
            this.consumer = consumer;
        }

        public void run(long currentTime) {
            if(currentTime > lastRunTime + interval)
            {
                consumer.accept(currentTime);
                lastRunTime = currentTime;
            }
        }
    }
}
