package io.kiw.speedy.publisher;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.exception.SpeedyMessagingNotInitiatedException;
import io.kiw.speedy.management.HostRegistrationMessage;
import io.kiw.speedy.management.ManagementKey;
import io.kiw.speedy.marshaller.OutboundDataHandler;
import io.kiw.speedy.subscriber.SpeedyMessageHandler;
import io.kiw.speedy.wiring.SpeedyWiring;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;


public class SpeedyMessagingPublisher implements PublishPromise
{
    private final OutboundDataHandler outboundDataHandler;
    private final SpeedyWiring wiring;
    private final SchedulerThread schedulerThread;
    private volatile boolean initialised;

    public SpeedyMessagingPublisher(OutboundDataHandler outboundDataHandler,
                                    SpeedyWiring wiring, SchedulerThread schedulerThread) {
        this.outboundDataHandler = outboundDataHandler;
        this.wiring = wiring;
        this.schedulerThread = schedulerThread;
    }

    public void hostRegistration(HostRegistrationMessage message, SpeedyConnection speedyConnection) {
        final String key = ManagementKey.HOST_REGISTRATION.getKey();
        ByteBuffer initiateHostRegistration = message.marshall();

        wiring.connectToRemoteHost(speedyConnection, () -> publish(key, initiateHostRegistration));
    }

    public void publish(String key, ByteBuffer data) {
        if(!initialised)
        {
            throw new SpeedyMessagingNotInitiatedException("Publisher Non initiated");
        }
        int eventId = wiring.startEvent();
        outboundDataHandler.handleData(key, data);

        wiring.completeEvent(eventId);
    }

    public void start()
    {
        enable();
    }

    public void enable() {
        initialised = true;
        schedulerThread.addFlushJob(new SchedulerThread.ScheduledJob(TimeUnit.MILLISECONDS.toNanos(5), wiring.getNanoTime(), (nanos) -> flush()));

    }

    public void resend(PublisherBucket sequenceBucket, long firstPacketKeyMissing, long lastPacketKeyMissing) {
        outboundDataHandler.resendData(sequenceBucket, firstPacketKeyMissing, lastPacketKeyMissing);
    }

    public void close() throws IOException {
        schedulerThread.stop();

        wiring.closePublisher();
    }

    public void flush() {
        outboundDataHandler.flushDataIfRequired();
    }
}
