package io.kiw.speedy.publisher;

import io.kiw.speedy.SpeedyHost;

import java.nio.ByteBuffer;

public interface PacketSender {
    void sendPacket(ByteBuffer writeBuffer, SpeedyHost socketAddress);
}
