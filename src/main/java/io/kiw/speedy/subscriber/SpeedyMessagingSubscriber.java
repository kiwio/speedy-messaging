package io.kiw.speedy.subscriber;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyMessagingImpl;
import io.kiw.speedy.channel.NackSchedulerJob;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.marshaller.MessageUnMarshaller;
import io.kiw.speedy.publisher.PublishPromise;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.wiring.SpeedyWiring;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousCloseException;
import java.util.concurrent.TimeUnit;

public class SpeedyMessagingSubscriber implements Runnable{
    private final MessageUnMarshaller messageUnmarshaller;
    private final ImmutableIntMap<SpeedyConnection> remoteConnections;
    private final PublishPromise publishPromise;
    private final ByteBuffer buffer;
    private final ByteBuffer[] buffers = new ByteBuffer[10];
    private final OnMessageErrorHandler onMessageErrorHandler;
    private final SpeedyWiring wiring;
    private final SchedulerThread schedulerThread;
    private final ImmutableIntMap<SpeedyMessageHandler> subscriptions;
    private final NackSchedulerJob nackSchedulerJob;

    public SpeedyMessagingSubscriber(MessageUnMarshaller messageUnMarshaller,
                                     ImmutableIntMap<SpeedyConnection> remoteConnections,
                                     PublishPromise publishPromise,
                                     OnMessageErrorHandler onMessageErrorHandler,
                                     SpeedyWiring wiring,
                                     NackSchedulerJob nackSchedulerJob,
                                     SchedulerThread schedulerThread,
                                     ImmutableIntMap<SpeedyMessageHandler> subscriptions) {
        this.messageUnmarshaller = messageUnMarshaller;
        this.remoteConnections = remoteConnections;
        this.publishPromise = publishPromise;
        this.onMessageErrorHandler = onMessageErrorHandler;
        this.wiring = wiring;
        this.schedulerThread = schedulerThread;
        this.subscriptions = subscriptions;
        allocateBuffers(buffers);
        this.buffer = ByteBuffer.allocateDirect(SpeedyMessagingImpl.DATAGRAM_LENGTH);
        this.wiring.registerFragmentHandler(this::handleFragment);
        this.nackSchedulerJob = nackSchedulerJob;
    }

    private static void allocateBuffers(ByteBuffer[] buffers) {
        for (int i = 0; i < buffers.length; i++) {
            buffers[i] = ByteBuffer.allocateDirect(SpeedyMessagingImpl.DATAGRAM_LENGTH);
        }
    }

    @Override
    public void run()
    {
        try {
            while (!Thread.interrupted()) {
                handleFragment();
            }
        }
        catch (AsynchronousCloseException e)
        {
            // elegantly finish thread
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }

        System.out.println("Closed Subscriber");
    }

    private void handleFragment() throws IOException {
//        buffer.clear();
//        wiring.receive(buffer);
//        buffer.flip();

        for (ByteBuffer byteBuffer : buffers) {
            byteBuffer.clear();
        }

        int received = wiring.receive(buffers);
        for (int i = 0; i < received; i++) {

            ByteBuffer buffer = buffers[i];
            buffer.flip();
            SubscriberChannelState channelState = handlePacket(buffer);
            if(channelState != null)
            {
                handleRecoveryMessages(channelState);
            }
        }
    }

    private void handleRecoveryMessages(SubscriberChannelState channelChannelState) {
        ByteBuffer bufferFromRecover;
        while((bufferFromRecover = channelChannelState.getNextRecoverMessage()).hasRemaining())
        {
            long packetSequenceNumber = bufferFromRecover.getLong();// packet channel number

            int publisherIdentifier = bufferFromRecover.getInt();
            bufferFromRecover.getInt(); // channel identifier we already know

            // execute messages from recovery packet
            while (bufferFromRecover.remaining() > 0)
            {
                messageUnmarshaller.unmarshallAndPotentiallyHandle(bufferFromRecover, subscriptions, onMessageErrorHandler, channelChannelState, publisherIdentifier, publishPromise);
            }

            // increment
            channelChannelState.increment(packetSequenceNumber);
        }
    }


    private SubscriberChannelState handlePacket(ByteBuffer buffer) {
        if(buffer.remaining() == 0)
        {
            return null;
        }
        long packetSequenceNumber = buffer.getLong();// packet sequence number

        int publisherIdentifier = buffer.getInt();
        SpeedyConnection speedyConnection = remoteConnections.get(publisherIdentifier);
        int channelIdentifier = buffer.getInt();
        SubscriberChannelState channelState = speedyConnection.getChannelSequenceState(channelIdentifier);
        long expectedPacketSequenceNumber = channelState.getPacketSequenceNumber();
        if(packetSequenceNumber == expectedPacketSequenceNumber)
        {

            // execute messages from packet
            while (buffer.remaining() > 0)
            {
                messageUnmarshaller.unmarshallAndPotentiallyHandle(buffer, subscriptions, onMessageErrorHandler, channelState, publisherIdentifier, publishPromise);
            }

            // increment
            channelState.increment(packetSequenceNumber);
        }
        else if(packetSequenceNumber > expectedPacketSequenceNumber)
        {
            buffer.rewind();

            if(expectedPacketSequenceNumber == 0)
            {
                channelState.setPacketSequenceNumber(packetSequenceNumber + 1);
                return handlePacket(buffer);
            }
            // NAK all that stuff
            channelState.copyToRecovery(buffer, packetSequenceNumber);
            nackSchedulerJob.onNack(channelIdentifier, publisherIdentifier, expectedPacketSequenceNumber, packetSequenceNumber  - 1);
            return null;
        }
        else if(packetSequenceNumber < expectedPacketSequenceNumber)
        {
            return null;
        }

        buffer.clear();
        return channelState;
    }

    public void close() throws IOException {
        wiring.closeSubscriber();
    }

    public void addNackScheduledJob() {
        this.schedulerThread.addNackJob(new SchedulerThread.ScheduledJob(TimeUnit.MICROSECONDS.toNanos(100), 0, nackSchedulerJob::onPulse));
    }
}
