package io.kiw.speedy.subscriber;

import java.nio.ByteBuffer;

public interface SpeedyMessageHandler {
    void handleMessage(ByteBuffer applicationData);
}
