package io.kiw.speedy.subscriber;

import io.kiw.tetryon.Tetryon;

import java.nio.ByteBuffer;

import static io.kiw.speedy.marshaller.OutboundDataHandlerImpl.MAX_EVENT_SIZE;

public class SubscriberConsumer implements Tetryon.EventHandler<HandleMessageEvent> {
    private final ByteBuffer eventBuffer = ByteBuffer.allocate(MAX_EVENT_SIZE);
    public SubscriberConsumer(String channelName) {

    }

    @Override
    public void handle(HandleMessageEvent handleMessageEvent) {
        handleMessageEvent.handle(eventBuffer);
    }
}
