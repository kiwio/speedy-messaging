package io.kiw.speedy.subscriber;

public interface EventPublisher<T> {
    void publishEvent(T event);
}
