package io.kiw.speedy.subscriber;

import io.kiw.speedy.publisher.PublishPromise;

import java.nio.ByteBuffer;

public class SubscriberSameThreadHandler implements SubscriberThreadHandler {

    public void handleMessage(SpeedyMessageHandler speedyMessageHandler, ByteBuffer eventBuffer, int publisherIdentifier, long eventSequenceNumber, PublishPromise publishPromise) {
        speedyMessageHandler.handleMessage(eventBuffer);
    }

    @Override
    public void close() {

    }
}
