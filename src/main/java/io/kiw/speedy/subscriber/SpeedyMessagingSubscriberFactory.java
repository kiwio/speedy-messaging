package io.kiw.speedy.subscriber;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.channel.NackSchedulerJob;
import io.kiw.speedy.exception.InvalidSubscriptionException;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.helper.ImmutableMapFactory;
import io.kiw.speedy.marshaller.MessageUnMarshaller;
import io.kiw.speedy.publisher.PublishPromise;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.wiring.SpeedyWiring;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import static io.kiw.speedy.subscriber.ManagementSubscriptions.*;

public class SpeedyMessagingSubscriberFactory {
    private final SpeedyHost localhost;
    private final MessageUnMarshaller messageUnMarshaller;
    private final ImmutableIntMap<SpeedyConnection> remoteConnections;
    private final PublishPromise publishPromise;
    private final OnMessageErrorHandler subcribeErrorHandler;
    private final SpeedyWiring wiring;
    private final ImmutableIntMap<PublisherBucket> publisherBuckets;
    private final SchedulerThread schedulerThread;
    private final Map<Integer, SpeedyMessageHandler> subscriptions = new HashMap<>();
    private final Set<Integer> channelIdentifiers;


    public SpeedyMessagingSubscriberFactory(SpeedyHost localhost,
                                            MessageUnMarshaller messageUnMarshaller,
                                            ImmutableIntMap<SpeedyConnection> remoteConnections,
                                            PublishPromise publishPromise,
                                            OnMessageErrorHandler subcribeErrorHandler,
                                            SpeedyWiring wiring,
                                            ImmutableIntMap<PublisherBucket> publisherBuckets,
                                            SchedulerThread schedulerThread, Set<Integer> channelIdentifiers) {
        this.localhost = localhost;
        this.messageUnMarshaller = messageUnMarshaller;
        this.remoteConnections = remoteConnections;
        this.publishPromise = publishPromise;
        this.subcribeErrorHandler = subcribeErrorHandler;
        this.wiring = wiring;
        this.publisherBuckets = publisherBuckets;
        this.schedulerThread = schedulerThread;
        this.channelIdentifiers = channelIdentifiers;
    }



    public SpeedyMessagingSubscriber build() {
        addManagementHandlers(subscriptions, localhost, remoteConnections, publishPromise, publisherBuckets);
        NackSchedulerJob nackSchedulerJob = NackSchedulerJobFactory.build(publishPromise, remoteConnections.keySet(), channelIdentifiers);
        return new SpeedyMessagingSubscriber(messageUnMarshaller, remoteConnections, publishPromise, subcribeErrorHandler, wiring, nackSchedulerJob, schedulerThread,
                ImmutableMapFactory.initialiseIntMap(subscriptions));
    }

    public void addSubscriptionHandler(String key, SpeedyMessageHandler speedyMessageHandler) {
        assertSubscriptionKeyHasBeenRegistered(key);

        subscriptions.put(key.hashCode(), speedyMessageHandler);
    }


    public void assertAllKeysHaveBeenSubscribedTo() {
        for (SpeedyConnection speedyConnection : remoteConnections.values()) {
            speedyConnection.assertAllKeysHaveBeenSubscribedTo(subscriptions.keySet());
        }

    }

    private static void addManagementHandlers(Map<Integer, SpeedyMessageHandler> subscriptions, SpeedyHost localhost, final ImmutableIntMap<SpeedyConnection> remoteHosts,
                                              PublishPromise publishPromise, ImmutableIntMap<PublisherBucket> publisherBuckets) {

        getHostRegistrationSubscription(subscriptions, localhost, remoteHosts, publishPromise);
        getHostAcknowledgementSubscription(subscriptions, localhost, remoteHosts);
        getNackSubscription(subscriptions, localhost, publishPromise, publisherBuckets);
    }

    private void assertSubscriptionKeyHasBeenRegistered(String key) {
        for (SpeedyConnection speedyConnection : remoteConnections.values()) {
            if(speedyConnection.containsRegistrationToKey(key))
            {
                return;
            }
        }

        throw new InvalidSubscriptionException(key);
    }

}
