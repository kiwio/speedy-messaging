package io.kiw.speedy.subscriber;

import io.kiw.speedy.channel.RecoveryBuffer;
import io.kiw.speedy.publisher.PublishPromise;

import java.nio.ByteBuffer;

import static io.kiw.speedy.marshaller.OutboundDataHandlerImpl.MAX_EVENT_SIZE;

public class SubscriberChannelState {
    private final SubscriberThreadHandler subscriberThreadHandler;
    private long packetSequenceNumber = 0;
    private final RecoveryBuffer recoveryBuffer;
    private final ByteBuffer eventBuffer = ByteBuffer.allocateDirect(MAX_EVENT_SIZE);
    private long eventSequenceNumber;

    public SubscriberChannelState(final int windowSize, final SubscriberThreadHandler subscriberThreadHandler) {
        this.recoveryBuffer = new RecoveryBuffer(windowSize);
        this.subscriberThreadHandler = subscriberThreadHandler;
    }

    public long getPacketSequenceNumber() {
        return packetSequenceNumber;
    }

    public ByteBuffer getNextRecoverMessage() {
        return recoveryBuffer.getPacket(packetSequenceNumber);
    }

    public void increment(long packetSequenceNumber) {
        recoveryBuffer.markSeen(packetSequenceNumber);
        this.packetSequenceNumber++;
    }

    public void copyToRecovery(ByteBuffer buffer, long packetSequenceNumber) {
        recoveryBuffer.addMessageToPacketRecoveryBuffer(packetSequenceNumber, buffer);
    }

    public ByteBuffer getEventBuffer() {
        return eventBuffer;
    }

    public void setPacketSequenceNumber(long packetSequenceNumber) {
        this.packetSequenceNumber = packetSequenceNumber;
    }




    public void setEventSequenceNumber(long eventSequenceNumber) {
        this.eventSequenceNumber = eventSequenceNumber;
    }

    public void handleMessage(SpeedyMessageHandler speedyMessageHandler, ByteBuffer eventBuffer, int publisherIdentifier, PublishPromise publishPromise) {
        subscriberThreadHandler.handleMessage(speedyMessageHandler, eventBuffer, publisherIdentifier, eventSequenceNumber, publishPromise);
    }

    public void close() {
        subscriberThreadHandler.close();
    }
}
