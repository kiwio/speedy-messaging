package io.kiw.speedy.subscriber;

import io.kiw.speedy.channel.NackSchedulerJob;
import io.kiw.speedy.channel.NackTask;
import io.kiw.speedy.helper.ImmutableMapFactory;
import io.kiw.speedy.publisher.PublishPromise;

import java.util.HashMap;
import java.util.Set;

public class NackSchedulerJobFactory {
    public static NackSchedulerJob build(PublishPromise publishPromise, Set<Integer> publisherIdentifiers, Set<Integer> channelIdentifiers) {

        HashMap<Long, NackTask> nackTasks = new HashMap<>();
        for (Integer channelIdentifier : channelIdentifiers) {
            for (Integer publisherIdentifier : publisherIdentifiers) {
                long key = ((long)channelIdentifier + publisherIdentifier);
                nackTasks.put(key, new NackTask(publishPromise, publisherIdentifier, channelIdentifier));
            }

        }

        return new NackSchedulerJob(ImmutableMapFactory.initialiseLongMap(nackTasks));
    }
}
