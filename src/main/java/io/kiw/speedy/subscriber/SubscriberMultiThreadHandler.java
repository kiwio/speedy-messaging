package io.kiw.speedy.subscriber;

import io.kiw.speedy.publisher.PublishPromise;
import io.kiw.speedy.wiring.MultiThreadSubscriberHandler;

import java.io.Closeable;
import java.nio.ByteBuffer;

public class SubscriberMultiThreadHandler implements SubscriberThreadHandler, Closeable
{
    private final HandleMessageEvent handleMessageEvent = new HandleMessageEvent();
    private final MultiThreadSubscriberHandler multiThreadSubscriberHandler;

    public SubscriberMultiThreadHandler(MultiThreadSubscriberHandler multiThreadSubscriberHandler) {
        this.multiThreadSubscriberHandler = multiThreadSubscriberHandler;
    }

    @Override
    public void handleMessage(SpeedyMessageHandler speedyMessageHandler, ByteBuffer eventBuffer, int publisherIdentifier, long eventSequenceNumber, PublishPromise publishPromise) {
        handleMessageEvent.overwrite(speedyMessageHandler, eventBuffer, publisherIdentifier, eventSequenceNumber, publishPromise);
        multiThreadSubscriberHandler.handleMessage(handleMessageEvent);
    }

    @Override
    public void close() {
        multiThreadSubscriberHandler.close();
    }
}
