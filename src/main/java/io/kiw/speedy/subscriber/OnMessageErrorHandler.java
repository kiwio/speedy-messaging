package io.kiw.speedy.subscriber;

public interface OnMessageErrorHandler {
    void handle(Throwable t);
}
