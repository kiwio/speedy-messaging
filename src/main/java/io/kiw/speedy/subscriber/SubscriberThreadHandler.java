package io.kiw.speedy.subscriber;

import io.kiw.speedy.publisher.PublishPromise;

import java.nio.ByteBuffer;

public interface SubscriberThreadHandler {
    void handleMessage(SpeedyMessageHandler speedyMessageHandler, ByteBuffer eventBuffer, int publisherIdentifier, long eventSequenceNumber, PublishPromise publishPromise);

    void close();
}
