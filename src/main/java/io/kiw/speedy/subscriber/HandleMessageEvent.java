package io.kiw.speedy.subscriber;

import io.kiw.speedy.publisher.PublishPromise;

import java.nio.ByteBuffer;

import static io.kiw.speedy.marshaller.OutboundDataHandlerImpl.MAX_EVENT_DATA_SIZE_PER_PACKET;

public class HandleMessageEvent {
    private SpeedyMessageHandler speedyMessageHandler;

    private int publisherIdentifier;
    private long eventSequenceNumber;
    private PublishPromise publishPromise;
    private final byte[]  data = new byte[MAX_EVENT_DATA_SIZE_PER_PACKET * 128];
    private int dataLength;

    public HandleMessageEvent overwrite(HandleMessageEvent instanceToReplaceWith) {
        this.speedyMessageHandler = instanceToReplaceWith.speedyMessageHandler;
        System.arraycopy(instanceToReplaceWith.data, 0, this.data, 0, instanceToReplaceWith.dataLength);
        this.dataLength = instanceToReplaceWith.dataLength;

        this.publisherIdentifier = instanceToReplaceWith.publisherIdentifier;
        this.eventSequenceNumber = instanceToReplaceWith.eventSequenceNumber;
        this.publishPromise = instanceToReplaceWith.publishPromise;
        return this;
    }

    public void handle(final ByteBuffer eventBuffer) {
        eventBuffer.clear();
        eventBuffer.put(data, 0, dataLength);
        eventBuffer.flip();
        speedyMessageHandler.handleMessage(eventBuffer);
    }

    public void overwrite(SpeedyMessageHandler speedyMessageHandler, ByteBuffer eventBuffer, int publisherIdentifier, long eventSequenceNumber, PublishPromise publishPromise) {
        this.speedyMessageHandler = speedyMessageHandler;

        eventBuffer.get(data, 0, eventBuffer.limit());
        this.dataLength = eventBuffer.position();
        this.publisherIdentifier = publisherIdentifier;
        this.eventSequenceNumber = eventSequenceNumber;
        this.publishPromise = publishPromise;
    }
}
