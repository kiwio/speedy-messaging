package io.kiw.speedy.subscriber;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.management.HostRegistrationMessage;
import io.kiw.speedy.management.ManagementKey;
import io.kiw.speedy.publisher.PublishPromise;

import java.util.Map;


public class ManagementSubscriptions {

    public static void getHostRegistrationSubscription(Map<Integer, SpeedyMessageHandler> subscriptions, SpeedyHost localhost, ImmutableIntMap<SpeedyConnection> remoteHosts, PublishPromise publishPromise) {
        subscriptions.put(ManagementKey.HOST_REGISTRATION.getKey().hashCode(), (message -> {
            HostRegistrationMessage hostRegistrationMessage = HostRegistrationMessage.unmarshall(message);
            if(hostRegistrationMessage.getReceiverHost().equals(localhost))
            {
                SpeedyHost senderHost = hostRegistrationMessage.getSenderHost();
                SpeedyConnection remoteHost = remoteHosts.get(senderHost.hashCode());
                if (remoteHost != null && !remoteHost.weHaveAcknowledgedThem()) {
                    // reply with subscriptions and host acknowledgement
                    publishPromise.publish(ManagementKey.HOST_ACKNOWLEDGEMENT.getKey(), new HostRegistrationMessage(localhost, senderHost).marshall());
                    remoteHost.weAcknowledgeThem();
                }
            }
        }));
    }

    public static void getHostAcknowledgementSubscription(Map<Integer, SpeedyMessageHandler> subscriptions, SpeedyHost localhost, ImmutableIntMap<SpeedyConnection> remoteHosts) {
        subscriptions.put(ManagementKey.HOST_ACKNOWLEDGEMENT.getKey().hashCode(), (message -> {
            HostRegistrationMessage hostRegistrationMessage = HostRegistrationMessage.unmarshall(message);
            int senderHostKey = hostRegistrationMessage.getSenderHost().hashCode();
            if(hostRegistrationMessage.getReceiverHost().equals(localhost) && !remoteHosts.get(senderHostKey).theyHaveAcknowledgedUs())
            {
                remoteHosts.get(senderHostKey).theyAcknowledgeUs();
            }
        }));
    }

    public static void getNackSubscription(Map<Integer, SpeedyMessageHandler> subscriptions, SpeedyHost localhost, PublishPromise publishPromise, ImmutableIntMap<PublisherBucket> publisherBuckets) {
        subscriptions.put(ManagementKey.NACK.getKey().hashCode(), (message -> {
            int originalPublisherIdentifier = message.getInt();
            int channelIdentifier = message.getInt();
            long firstPacketKeyMissing = message.getLong();
            long lastPacketKeyMissing = message.getLong();
            if(originalPublisherIdentifier == localhost.hashCode())
            {
                PublisherBucket publisherBucket = publisherBuckets.get(channelIdentifier);
                System.out.println("PUBLISHER: received NACK - resending channel " + channelIdentifier + "(current sequence number " +
                        publisherBucket.getPublisherSequenceState().getPacketSequenceNumber()+ ")" +
                        " range " + firstPacketKeyMissing + " to " + lastPacketKeyMissing);

                publishPromise.resend(publisherBucket, firstPacketKeyMissing, lastPacketKeyMissing);
            }
        }));
    }

    public static String getChannelName() {
        return "managementChannel";
    }
}
