package io.kiw.speedy;

import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.management.HostRegistrationMessage;
import io.kiw.speedy.marshaller.MessageUnMarshaller;
import io.kiw.speedy.marshaller.OutboundDataHandler;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.publisher.SpeedyMessagingPublisher;
import io.kiw.speedy.subscriber.*;
import io.kiw.speedy.wiring.SpeedyWiring;
import io.kiw.speedy.wiring.thread.ThreadHandler;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Set;

public class SpeedyMessagingImpl implements SpeedyMessaging {
    public static final int DATAGRAM_LENGTH = 1480 * 4;
    private SpeedyMessagingSubscriber speedyMessagingSubscriber;
    private final SpeedyMessagingPublisher speedyMessagingPublisher;
    private final SpeedyHost localhost;
    private final ImmutableIntMap<PublisherBucket> publisherBuckets;
    private final ImmutableIntMap<SpeedyConnection> remoteConnections;
    private final SchedulerThread schedulerThread;
    private final SpeedyWiring wiring;
    private SpeedyMessagingSubscriberFactory speedyMessagingSubscriberFactory;

    public SpeedyMessagingImpl(final SpeedyHost localhost,
                               OnMessageErrorHandler subcribeErrorHandler,
                               SpeedyWiring wiring,
                               ImmutableIntMap<PublisherBucket> publisherBuckets,
                               ImmutableIntMap<SpeedyConnection> remoteConnections,
                               OutboundDataHandler outboundDataHandler,
                               SchedulerThread schedulerThread,
                               Set<Integer> channelIdentifiers) {
        this.wiring = wiring;
        this.localhost = localhost;
        this.publisherBuckets = publisherBuckets;
        this.remoteConnections = remoteConnections;
        this.schedulerThread = schedulerThread;

        speedyMessagingPublisher = new SpeedyMessagingPublisher(outboundDataHandler, wiring, schedulerThread);
        speedyMessagingSubscriberFactory = new SpeedyMessagingSubscriberFactory(localhost, new MessageUnMarshaller(), remoteConnections, speedyMessagingPublisher,
                subcribeErrorHandler, wiring, publisherBuckets, schedulerThread, channelIdentifiers);
    }

    public void start() {
        speedyMessagingSubscriber = speedyMessagingSubscriberFactory.build();
        assertAllKeysHaveBeenSubscribedTo();
        wiring.start(speedyMessagingPublisher, speedyMessagingSubscriber, schedulerThread, remoteConnections);
        connectToRemoteHosts();
    }



    @Override
    public void subscribe(String key, SpeedyMessageHandler messageHandler) {
        speedyMessagingSubscriberFactory.addSubscriptionHandler(key, messageHandler);
    }

    @Override
    public void publish(String key, ByteBuffer bytes) {
        speedyMessagingPublisher.publish(key, bytes);
    }

    @Override
    public void close() throws IOException {
        speedyMessagingPublisher.flush();
        if(speedyMessagingSubscriber != null)
        {
            speedyMessagingSubscriber.close();
        }
        speedyMessagingPublisher.close();
        remoteConnections.values().forEach(SpeedyConnection::close);
    }

    private void connectToRemoteHosts() {
        ThreadHandler threadHandler = wiring.getThreadHandler();
        for (SpeedyConnection remoteHost : remoteConnections.values().toArray(new SpeedyConnection[remoteConnections.size()])) {
            speedyMessagingPublisher.hostRegistration(new HostRegistrationMessage(localhost, remoteHost.getHost()), remoteHost);
        }
        threadHandler.join();
        wiring.connectIfSingleConnection(remoteConnections);
    }

    private void assertAllKeysHaveBeenSubscribedTo() {
        speedyMessagingSubscriberFactory.assertAllKeysHaveBeenSubscribedTo();
    }
}
