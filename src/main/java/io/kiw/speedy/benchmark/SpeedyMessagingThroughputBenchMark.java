package io.kiw.speedy.benchmark;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.SpeedyMessagingImpl;
import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetwork;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;
import io.kiw.speedy.wiring.LinuxUdpSpeedyWiring;
import io.kiw.speedy.wiring.StubRouter;

import java.nio.ByteBuffer;
import java.util.concurrent.TimeUnit;

public class SpeedyMessagingThroughputBenchMark {

    public static final int messagesCount = 100000000;
    public static final int WINDOW_SIZE = 32_768;
    public static final String KEY = "ping";
    private static ByteBuffer BYTES = ByteBuffer.allocateDirect(40);

    public static void main(String[] args) throws InterruptedException {
        SpeedyHost pubHost = new SpeedyHost("pub", "127.0.0.1", 9000);
        SpeedyHost subHost = new SpeedyHost("sub", "127.0.0.1", 9001);

        StubRouter stubRouter = new StubRouter();
        SpeedyNetwork speedyNetwork = new SpeedyNetworkBuilder().defaultWindowSize(WINDOW_SIZE)
                .addChannel(SpeedyChannel.newChannel("ping").withPublishers(pubHost).forKeys("ping").toSubscribers(subHost)).buildNetwork();


//        SpeedyMessagingImpl pub = speedyNetwork.createInstanceBuilderFromPerspective(pubHost).build();
        SpeedyMessagingImpl pub = speedyNetwork.createInstanceBuilderFromPerspective(pubHost).withWiring(new LinuxUdpSpeedyWiring(pubHost.getPort())).build();
//        SpeedyMessagingImpl pub = speedyNetwork.createInstanceBuilderFromPerspective(pubHost).withWiring(new StubWiring(stubRouter, pubHost)).build();



        pub.start();
        stubRouter.start();
        System.out.println("Connected");
        for (int i = 0; i < 1000; i++) {
            runBenchMark(pub);
        }
    }

    private static void runBenchMark(SpeedyMessagingImpl pub) {
        final long startTime = System.nanoTime();
        for (int i = 0; i < messagesCount; i++) {
            BYTES.position(0);
            BYTES.limit(40);
            pub.publish(KEY, BYTES);
        }
        final long endTime = System.nanoTime();
        outTimes(endTime - startTime);
    }

    private static void outTimes(long timeTaken) {
        long timeTakenPerMessageAvg = timeTaken / messagesCount;

        long msgsPerSecond = TimeUnit.SECONDS.toNanos(1) / timeTakenPerMessageAvg;
        System.out.println("published " + messagesCount + " messages in " + timeTaken + "ns. That's an average of " + timeTakenPerMessageAvg + "ns a message which is " + msgsPerSecond + "msg/s");
    }


}
