package io.kiw.speedy.benchmark;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.SpeedyMessagingImpl;
import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetwork;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;
import io.kiw.speedy.wiring.LinuxUdpSpeedyWiring;
import io.kiw.speedy.wiring.StubRouter;

import static io.kiw.speedy.benchmark.SpeedyMessagingThroughputBenchMark.WINDOW_SIZE;

public class SpeedyMessagingThroughputSubscriber {


    public static void main(String[] args) throws InterruptedException {
        SpeedyHost pubHost = new SpeedyHost("pub", "127.0.0.1", 9000);
        SpeedyHost subHost = new SpeedyHost("sub", "127.0.0.1", 9001);

        StubRouter stubRouter = new StubRouter();
        SpeedyNetwork speedyNetwork = new SpeedyNetworkBuilder().defaultWindowSize(WINDOW_SIZE)
                .addChannel(SpeedyChannel.newChannel("ping").withPublishers(pubHost).forKeys("ping").toSubscribers(subHost)).buildNetwork();


//        SpeedyMessagingImpl sub = speedyNetwork.createInstanceBuilderFromPerspective(subHost).build();
        SpeedyMessagingImpl sub = speedyNetwork.createInstanceBuilderFromPerspective(subHost).withWiring(new LinuxUdpSpeedyWiring(subHost.getPort())).build();
//        SpeedyMessagingImpl sub = speedyNetwork.createInstanceBuilderFromPerspective(subHost).withWiring(new StubWiring(stubRouter, pubHost)).build();


        sub.subscribe("ping", m -> {

        });

        sub.start();
        stubRouter.start();
    }


}
