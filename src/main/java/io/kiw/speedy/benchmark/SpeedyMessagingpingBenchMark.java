package io.kiw.speedy.benchmark;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.SpeedyMessagingImpl;
import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetwork;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;
import io.kiw.speedy.wiring.StubRouter;

import java.nio.ByteBuffer;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

import static io.kiw.speedy.parallel.ParallelHelper.parallel;

public class SpeedyMessagingpingBenchMark {

    public static final int ITERATIONS = 100000;
    public static final int WINDOW_SIZE = 65536;

    public static void main(String[] args) throws InterruptedException {
        SpeedyHost pubHost = new SpeedyHost("pub", "127.0.0.1", 9000);
        SpeedyHost subHost = new SpeedyHost("sub", "127.0.0.1", 9001);

        StubRouter stubRouter = new StubRouter();
        SpeedyNetwork speedyNetwork = new SpeedyNetworkBuilder().defaultWindowSize(WINDOW_SIZE)
                .addChannel(SpeedyChannel.newChannel("ping").withPublishers(pubHost).forKeys("ping").toSubscribers(subHost))
                .addChannel(SpeedyChannel.newChannel("pong").withPublishers(subHost).forKeys("pong").toSubscribers(pubHost))
        .buildNetwork();


        SpeedyMessagingImpl pub = speedyNetwork.createInstanceBuilderFromPerspective(pubHost).build();
        SpeedyMessagingImpl sub = speedyNetwork.createInstanceBuilderFromPerspective(subHost).build();
//        SpeedyMessagingImpl pub = speedyNetwork.createInstanceBuilderFromPerspective(pubHost).withWiring(new StubWiring(stubRouter, pubHost)).build();
//        SpeedyMessagingImpl sub = speedyNetwork.createInstanceBuilderFromPerspective(subHost).withWiring(new StubWiring(stubRouter, pubHost)).build();


        sub.subscribe("ping", (m) -> {
            sub.publish("pong", m);
        });

        long[] times = new long[ITERATIONS];


        pub.subscribe("pong", (m) -> {
            long pingTime = m.getLong();
            int index = m.getInt();
            populate(times, pingTime, index);
        });

        ByteBuffer publisherBytes = ByteBuffer.allocate(12);

        parallel(pub::start, sub::start);
        stubRouter.start();
        System.out.println("Connected");

        for (int i = 0; i < 100; i++) {
            runBenchmark(pub, times, publisherBytes);
        }
    }

    private static void clearTimes(long[] times) {
        for (int i = 0; i < times.length; i++) {
            times[i] = 0;
        }
    }

    public static void runBenchmark(SpeedyMessagingImpl pub, long[] times, ByteBuffer publisherBytes) throws InterruptedException {
        final AtomicInteger count = new AtomicInteger(0);

        for (int i = 0; i < ITERATIONS; i++) {
            if(i % 10 == 0)
            {
                Thread.sleep(1);
            }
            echo(pub, count, publisherBytes);
        }

        Arrays.sort(times);
        Long twoNines = times[(int) (times.length * 0.99)] /1000;
        Long threeNines = times[(int) (times.length * 0.999)]/ 1000;
        Long fourNines = times[(int) (times.length * 0.9999)]/ 1000;
        Long max = times[times.length - 1] /1000;
        long sum = Arrays.stream(times).reduce((aLong, aLong2) -> aLong + aLong2).getAsLong();

        long mean = (sum / times.length) / 1000;
        System.out.println("two nines:  " + twoNines + "us");
        System.out.println("three nines:  " + threeNines + "us");
        System.out.println("four nines:  " + fourNines + "us");
        System.out.println("max:  " + max + "us");
        System.out.println("mean: " + mean + "us");
        System.out.println("");
        System.out.println("");
        clearTimes(times);

    }

    private static void echo(SpeedyMessagingImpl pub, AtomicInteger count, ByteBuffer publisherBytes) {
        long pingTime = System.nanoTime();
        int index = count.getAndIncrement();
        publisherBytes.clear();
        publisherBytes.putLong(pingTime);
        publisherBytes.putInt(index);
        publisherBytes.flip();
        pub.publish("ping", publisherBytes);
    }

    private static void populate(long[] times, long pingTime, int index) {
        long pongTime = System.nanoTime();
        long echoTimeNanos = pongTime - pingTime;
        times[index] = (echoTimeNanos);
    }

}
