package io.kiw.speedy;

import io.kiw.speedy.channel.PublisherSequenceState;

import java.nio.ByteBuffer;
import java.util.Set;

public class PublisherBucket {
    private final PublisherSequenceState packetPublisherSequenceState;
    private final Set<String> keysToPublishTo;
    private final SpeedyConnection[] speedyConnections;

    public PublisherBucket(Set<String> keysToPublishTo, int windowSize, int localhostIdentifier, int channelIdentifier, SpeedyConnection[] speedyConnections) {
        this.speedyConnections = speedyConnections;
        this.keysToPublishTo = keysToPublishTo;
        packetPublisherSequenceState = PublisherSequenceState.initialise(windowSize,localhostIdentifier, channelIdentifier);
    }

    public SpeedyConnection[] getRemoteConnections() {
        return speedyConnections;
    }

    public boolean hasKey(String key) {
        return keysToPublishTo.contains(key);
    }

    public ByteBuffer getPacketBuffer() {
        return packetPublisherSequenceState.getCurrentPublisherBuffer();
    }

    public PublisherSequenceState getPublisherSequenceState() {
        return packetPublisherSequenceState;
    }
}
