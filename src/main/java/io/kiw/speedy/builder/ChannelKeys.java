package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.exception.ConflictingRouteException;
import io.kiw.speedy.management.ManagementKey;

public class ChannelKeys {
    private final String channelName;
    private final SpeedyHost[] publisherHost;

    public ChannelKeys(String channelName, SpeedyHost[] publisherHost) {
        this.channelName = channelName;

        this.publisherHost = publisherHost;
    }

    public RouteSubscribers forKeys(String... keys) {
        for (String key : keys) {
            if(ManagementKey.isManagementKey(key))
            {
                throw new ConflictingRouteException("Specified key '" + key + "' conflicts with a management key, please use something else");
            }
        }

        return new RouteSubscribers(channelName, publisherHost, keys);
    }
}
