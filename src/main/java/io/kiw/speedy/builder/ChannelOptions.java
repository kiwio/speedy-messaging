package io.kiw.speedy.builder;

import java.util.Optional;

import static io.kiw.speedy.builder.BuilderValidation.validateWindowSize;

public class ChannelOptions {

    private Optional<Integer> windowSize = Optional.empty();
    private int threadCount = 1;

    public static ChannelOptions defaultOptions() {
        return new ChannelOptions();
    }

    public ChannelOptions withWindowSize(int windowSize) {
        validateWindowSize(windowSize);
        this.windowSize = Optional.of(windowSize);
        return this;
    }

    public ChannelOptions withSubscriberThreads(int threadCount) {
        this.threadCount = threadCount;
        return this;
    }

    private ChannelOptions() {
    }

    Optional<Integer> getWindowSize() {
        return windowSize;
    }


    int getSubscriberThreadCount() {
        return threadCount;
    }
}
