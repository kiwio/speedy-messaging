package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;

import java.util.Collection;

public class SpeedyChannel {
    final String channelName;
    final Collection<SpeedyHost> publisherHosts;
    final Collection<String> keys;
    final Collection<SpeedyHost> subscriberHosts;
    private ChannelOptions channelOptions = ChannelOptions.defaultOptions();

    SpeedyChannel(String channelName, Collection<SpeedyHost> publisherHosts, Collection<String> keys, Collection<SpeedyHost> subscriberHosts) {
        this.channelName = channelName;
        this.publisherHosts = publisherHosts;
        this.keys = keys;
        this.subscriberHosts = subscriberHosts;
    }

    public static ChannelPublishers newChannel(String channelName) {
        return new ChannelPublishers(channelName);
    }


    public SpeedyChannel withOptions(ChannelOptions channelOptions) {
        this.channelOptions = channelOptions;
        return this;
    }

    ChannelOptions getChannelOptions() {
        return channelOptions;
    }

    @Override
    public String toString() {
        return "SpeedyChannel{" +
                "publisherHosts=" + publisherHosts +
                ", keys=" + keys +
                ", subscriberHosts=" + subscriberHosts +
                '}';
    }
}
