package io.kiw.speedy.builder;

public enum ChannelMode {
    PUBLISHER_ONLY, SUBSCRIBER_ONLY, DUPLEX
}
