package io.kiw.speedy.builder;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.SpeedyMessagingImpl;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.helper.ImmutableMapFactory;
import io.kiw.speedy.marshaller.EventMarshaller;
import io.kiw.speedy.marshaller.OutboundDataHandlerImpl;
import io.kiw.speedy.publisher.PacketFlusher;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.subscriber.*;
import io.kiw.speedy.wiring.MultiThreadSubscriberHandler;
import io.kiw.speedy.wiring.SpeedyWiring;
import io.kiw.speedy.wiring.UdpSpeedyWiring;
import io.kiw.tetryon.EventHandlerGroup;
import io.kiw.tetryon.Tetryon;

import java.util.*;

public class SpeedyMessagingFactory {

    private final SpeedyHost localhost;
    private OnMessageErrorHandler onMessageErrorHandler = t -> {};
    private SpeedyWiring wiring;
    private final Map<Integer, SubscribingChannel> subscribingChannels;
    private final Map<Integer, PublishingChannel> publishingChannels;
    private final Set<SpeedyHost> remoteHosts;

    SpeedyMessagingFactory(SpeedyHost localhost,
                           Map<Integer, PublishingChannel> publishingChannels,
                           Map<Integer, SubscribingChannel> subscribingChannels,
                           Set<SpeedyHost> remoteHosts) {
        this.localhost = localhost;
        this.publishingChannels = publishingChannels;
        this.subscribingChannels = subscribingChannels;
        this.remoteHosts = remoteHosts;
    }

    public SpeedyMessagingFactory withOnMessageErrorHandler(OnMessageErrorHandler onMessageErrorHandler) {
        this.onMessageErrorHandler = onMessageErrorHandler;
        return this;
    }

    public SpeedyMessagingFactory withWiring(SpeedyWiring wiring) {
        this.wiring = wiring;
        return this;
    }

    public SpeedyMessagingImpl build() {
        if(localhost == null)
        {
            throw new RuntimeException("Local speedy host must be defined.");
        }

        if(wiring == null)
        {
            wiring = new UdpSpeedyWiring(localhost.getPort());
        }

        SchedulerThread schedulerThread = new SchedulerThread(wiring::getNanoTime);
        wiring.addPulseHandler(schedulerThread::pulse);

        ImmutableIntMap<SpeedyConnection> remoteConnections = buildRemoteConnections();
        ImmutableIntMap<PublisherBucket> publisherBuckets = createPublisherBuckets(publishingChannels, remoteConnections);

        PacketFlusher packetflusher = new PacketFlusher(wiring::sendPacket);

        return new SpeedyMessagingImpl(
                localhost,
                onMessageErrorHandler,
                wiring,
                publisherBuckets,
                remoteConnections,
                wiring.wrapPacketHandler(
                        new OutboundDataHandlerImpl(
                                packetflusher,
                                new EventMarshaller(packetflusher),
                                publisherBuckets.values().toArray(new PublisherBucket[publisherBuckets.size()]),
                            wiring)
                ),
                schedulerThread,
                subscribingChannels.keySet());
    }

    private ImmutableIntMap<SpeedyConnection> buildRemoteConnections() {
        Map<Integer, SpeedyConnection> remoteConnections = new HashMap<>();
        for (SpeedyHost remoteHost : remoteHosts) {
            remoteConnections.put(remoteHost.hashCode(),
                    new SpeedyConnection(
                            remoteHost,
                            collectRegisteredKeys(),
                            buildChannelStateForRemoteHost(remoteHost)));
        }
        return ImmutableMapFactory.initialiseIntMap(remoteConnections);
    }

    private Set<Integer> collectRegisteredKeys() {
        Set<Integer> registeredKeyIdentifiers = new HashSet<>();
        for (SubscribingChannel subscribingChannel : subscribingChannels.values()) {
            for (String subscribingKey : subscribingChannel.getKeys()) {
                registeredKeyIdentifiers.add(subscribingKey.hashCode());
            }
        }
        return registeredKeyIdentifiers;
    }

    private ImmutableIntMap<SubscriberChannelState> buildChannelStateForRemoteHost(SpeedyHost remoteHost) {
        Map<Integer, SubscriberChannelState> channelStateForRemoteHost = new HashMap<>();

        for (Map.Entry<Integer, SubscribingChannel> subscribingChannelEntry : subscribingChannels.entrySet()) {

            SubscribingChannel subscribingChannel = subscribingChannelEntry.getValue();
            PublishingChannel publishingChannel = publishingChannels.get(subscribingChannelEntry.getKey());
            if(subscribingChannel.getPublishingHosts().contains(remoteHost) || publishingChannel.getSubscribingHosts().contains(remoteHost))
            {
                SubscriberThreadHandler subscriberThreadHandler;
                if (subscribingChannel.getSubscriberThreads() <= 1)
                {
                    subscriberThreadHandler = new SubscriberSameThreadHandler();
                }
                else
                {
                    List<Tetryon.EventHandler<HandleMessageEvent>> group = EventHandlerGroup.createGroup(() ->
                            new SubscriberConsumer(subscribingChannelEntry.getValue().getName()), subscribingChannel.getSubscriberThreads());

                    MultiThreadSubscriberHandler multiThreadSubscriberHandler = wiring.buildMultiThreadSubscriberHandler(group);
                    subscriberThreadHandler = new SubscriberMultiThreadHandler(multiThreadSubscriberHandler);

                }

                SubscriberChannelState subscriberChannelState = new SubscriberChannelState(
                        subscribingChannel.getWindowSizeOfRoute(),
                        subscriberThreadHandler
                        );
                channelStateForRemoteHost.put(subscribingChannelEntry.getKey(), subscriberChannelState);
            }
        }

        return ImmutableMapFactory.initialiseIntMap(channelStateForRemoteHost);
    }


    private ImmutableIntMap<PublisherBucket> createPublisherBuckets(Map<Integer, PublishingChannel> publishingChannels,
                                                                    ImmutableIntMap<SpeedyConnection> allRemoteConnections) {
        Map<Integer, PublisherBucket> publisherBuckets = new HashMap<>();



        for (Map.Entry<Integer, PublishingChannel> publishingChannelEntry : publishingChannels.entrySet()) {

            PublishingChannel publishingChannel = publishingChannelEntry.getValue();
            HashSet<String> keysToPublishto = new HashSet<>();
            keysToPublishto.addAll(publishingChannel.getKeys());

            publisherBuckets.put(publishingChannelEntry.getKey(),
                    new PublisherBucket(
                            keysToPublishto,
                            publishingChannel.getWindowSizeOfRoute(),
                            localhost.hashCode(),
                            publishingChannelEntry.getKey(),
                            getFilteredSpeedyConnections(publishingChannel.getSubscribingHosts(), allRemoteConnections)
                    ));
        }

        return ImmutableMapFactory.initialiseIntMap(publisherBuckets);
    }

    private SpeedyConnection[] getFilteredSpeedyConnections(Collection<SpeedyHost> publishingHosts, ImmutableIntMap<SpeedyConnection> allRemoteConnections) {
        Set<SpeedyConnection> speedyConnections = new HashSet<>();
        for (SpeedyHost publishingHost : publishingHosts) {
            speedyConnections.add(allRemoteConnections.get(publishingHost.hashCode()));
        }

        return speedyConnections.toArray(new SpeedyConnection[speedyConnections.size()]);
    }
}
