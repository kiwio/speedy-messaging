package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.management.ManagementKey;
import io.kiw.speedy.subscriber.ManagementSubscriptions;

import java.util.*;


public class SpeedyNetwork {
    public static final int MANAGEMENT_ROUTE_WINDOW_SIZE = 256;
    private final List<SpeedyChannel> speedyChannels;
    private final int defaultWindowSize;

    public SpeedyNetwork(List<SpeedyChannel> speedyChannels, int defaultWindowSize) {

        this.speedyChannels = speedyChannels;
        this.defaultWindowSize = defaultWindowSize;
    }

    public SpeedyMessagingFactory createInstanceBuilderFromPerspective(SpeedyHost perspectiveHost) {
        final Map<Integer, PublishingChannel> publishingChannels = new HashMap<>();
        final Map<Integer, SubscribingChannel> subscribingChannels = new HashMap<>();
        final Set<SpeedyHost> remoteHosts = new HashSet<>();
        for (SpeedyChannel speedyChannel : speedyChannels) {
            final String channelName = speedyChannel.channelName;
            ChannelOptions channelOptions = speedyChannel.getChannelOptions();
            final int windowSizeOfChannel = channelOptions.getWindowSize().isPresent() ? channelOptions.getWindowSize().get() : defaultWindowSize;
            final int subscriberThreads = channelOptions.getSubscriberThreadCount();
            if(iAmAPublisherAndASubscriberOnThisChannel(perspectiveHost, speedyChannel))
            {
                // all hosts on channel are remote hosts
                remoteHosts.addAll(speedyChannel.subscriberHosts);
                remoteHosts.addAll(speedyChannel.publisherHosts);

                // add as publisher
                publishingChannels.put(channelName.hashCode(), new PublishingChannel(channelName, speedyChannel.keys, speedyChannel.subscriberHosts, windowSizeOfChannel, ChannelMode.DUPLEX));

                // add as subscriber
                subscribingChannels.put(channelName.hashCode(), new SubscribingChannel(speedyChannel.keys, speedyChannel.publisherHosts, windowSizeOfChannel, channelName, subscriberThreads));
            }
            else if(iAmAPublisherOnlyOnThisChannel(perspectiveHost, speedyChannel))
            {
                // only add subscriber hosts as we won't publish to other publishers
                remoteHosts.addAll(speedyChannel.subscriberHosts);

                publishingChannels.put(channelName.hashCode(), new PublishingChannel(channelName, speedyChannel.keys, speedyChannel.subscriberHosts, windowSizeOfChannel, ChannelMode.PUBLISHER_ONLY));
                subscribingChannels.put(channelName.hashCode(),
                        new SubscribingChannel(
                                Collections.emptyList(),
                                speedyChannel.subscriberHosts, windowSizeOfChannel, channelName, subscriberThreads));
            }
            else if (iAmASubscriberOnlyOnThisChannel(perspectiveHost, speedyChannel))
            {
                // only add publisher hosts as we won't interact with any other subscribers on the channel
                remoteHosts.addAll(speedyChannel.publisherHosts);

                publishingChannels.put(channelName.hashCode(), new PublishingChannel(channelName, Collections.emptyList(), speedyChannel.publisherHosts, windowSizeOfChannel, ChannelMode.SUBSCRIBER_ONLY));
                subscribingChannels.put(channelName.hashCode(), new SubscribingChannel(speedyChannel.keys, speedyChannel.publisherHosts, windowSizeOfChannel, channelName, subscriberThreads));
            }
        }

        registerManagementChannel(perspectiveHost, publishingChannels, subscribingChannels, remoteHosts);

        return new SpeedyMessagingFactory(perspectiveHost, publishingChannels, subscribingChannels, remoteHosts);
    }

    private boolean iAmASubscriberOnlyOnThisChannel(SpeedyHost perspectiveHost, SpeedyChannel speedyChannel) {
        return !speedyChannel.publisherHosts.contains(perspectiveHost) && speedyChannel.subscriberHosts.contains(perspectiveHost);
    }

    private boolean iAmAPublisherOnlyOnThisChannel(SpeedyHost perspectiveHost, SpeedyChannel speedyChannel) {
        return speedyChannel.publisherHosts.contains(perspectiveHost) && !speedyChannel.subscriberHosts.contains(perspectiveHost);
    }

    private boolean iAmAPublisherAndASubscriberOnThisChannel(SpeedyHost perspectiveHost, SpeedyChannel speedyChannel) {
        return speedyChannel.publisherHosts.contains(perspectiveHost) && speedyChannel.subscriberHosts.contains(perspectiveHost);
    }

    private void registerManagementChannel(SpeedyHost perspectiveHost, Map<Integer, PublishingChannel> publishingChannels, Map<Integer, SubscribingChannel> subscribingChannels, Set<SpeedyHost> remoteHosts) {
        Set<String> allStaticManagentKeys = ManagementKey.MANAGEMENT_KEY_SET;

        HashSet<String> allManagementSubscriptionKeys = new HashSet<>(allStaticManagentKeys);

        HashSet<String> allManagementPublisherKeys = new HashSet<>(allStaticManagentKeys);

        publishingChannels.put(ManagementSubscriptions.getChannelName().hashCode(), new PublishingChannel(ManagementSubscriptions.getChannelName(), allManagementPublisherKeys, remoteHosts, MANAGEMENT_ROUTE_WINDOW_SIZE, ChannelMode.DUPLEX));
        subscribingChannels.put(ManagementSubscriptions.getChannelName().hashCode(), new SubscribingChannel(allManagementSubscriptionKeys, remoteHosts, MANAGEMENT_ROUTE_WINDOW_SIZE, ManagementSubscriptions.getChannelName(), 1));
    }


}
