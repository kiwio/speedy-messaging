package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;

import java.util.Collection;

public class SubscribingChannel {
    private final Collection<String> keys;
    private final Collection<SpeedyHost> publishingHosts;
    private int windowSizeOfRoute;
    private final String channelName;
    private int subscriberThreads;

    public SubscribingChannel(Collection<String> keys, Collection<SpeedyHost> publishingHosts, int windowSizeOfRoute, String channelName, int subscriberThreads) {
        this.keys = keys;
        this.publishingHosts = publishingHosts;
        this.windowSizeOfRoute = windowSizeOfRoute;
        this.channelName = channelName;
        this.subscriberThreads = subscriberThreads;
    }

    public Collection<String> getKeys() {
        return keys;
    }

    public Collection<SpeedyHost> getPublishingHosts() {
        return publishingHosts;
    }

    public int getWindowSizeOfRoute() {
        return windowSizeOfRoute;
    }

    public int getSubscriberThreads() {
        return subscriberThreads;
    }

    public String getName() {
        return channelName;
    }

    @Override
    public String toString() {
        return "SubscribingChannel{" +
                "keys=" + keys +
                ", publishingHosts=" + publishingHosts +
                ", windowSizeOfRoute=" + windowSizeOfRoute +
                ", channelName='" + channelName + '\'' +
                ", subscriberThreads=" + subscriberThreads +
                '}';
    }
}
