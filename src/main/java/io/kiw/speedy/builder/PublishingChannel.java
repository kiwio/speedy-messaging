package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;

import java.util.Collection;

public class PublishingChannel {
    private final String channelName;
    private final Collection<String> keys;
    private final Collection<SpeedyHost> speedyHosts;
    private final int windowSizeOfRoute;
    private final ChannelMode channelMode;

    public PublishingChannel(String channelName, Collection<String> keys, Collection<SpeedyHost> speedyHosts, int windowSizeOfRoute, ChannelMode channelMode) {
        this.channelName = channelName;
        this.keys = keys;
        this.speedyHosts = speedyHosts;
        this.windowSizeOfRoute = windowSizeOfRoute;
        this.channelMode = channelMode;
    }

    public Collection<String> getKeys() {
        return keys;
    }

    public Collection<SpeedyHost> getSubscribingHosts() {
        return speedyHosts;
    }

    public int getWindowSizeOfRoute() {
        return windowSizeOfRoute;
    }

    @Override
    public String toString() {
        return "PublishingChannel{" +
                "keys=" + keys +
                ", speedyHosts=" + speedyHosts +
                '}';
    }

    public ChannelMode getChannelMode() {
        return channelMode;
    }

    public String getName() {
        return channelName;
    }
}
