package io.kiw.speedy.builder;

public class BuilderValidation {
    static void validateWindowSize(int windowSize) {
        boolean isValid = false;
        for (int i = 2; i <= 8388608; i = i << 1) {
            if(windowSize == i)
            {
                isValid = true;
            }
        }

        if(!isValid)
        {
            throw new IllegalArgumentException("Window size must be divisible by two");
        }
    }
}
