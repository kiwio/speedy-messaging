package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;

public class ChannelPublishers {

    private final String channelName;

    public ChannelPublishers(final String channelName) {
        this.channelName = channelName;

    }
    public ChannelKeys withPublishers(SpeedyHost... publishers) {
        return new ChannelKeys(channelName, publishers);
    }
}
