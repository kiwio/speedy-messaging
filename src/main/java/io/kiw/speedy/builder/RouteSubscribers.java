package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;

import java.util.Arrays;

public class RouteSubscribers {
    private final String channelName;
    private final SpeedyHost[] publisherHosts;
    private final String[] keys;

    public RouteSubscribers(String channelName, SpeedyHost[] publisherHosts, String[] keys) {
        this.channelName = channelName;
        this.publisherHosts = publisherHosts;
        this.keys = keys;
    }

    public SpeedyChannel toSubscribers(SpeedyHost... subscriberHosts) {
       return new SpeedyChannel(channelName, Arrays.asList(publisherHosts), Arrays.asList(keys), Arrays.asList(subscriberHosts));
    }
}
