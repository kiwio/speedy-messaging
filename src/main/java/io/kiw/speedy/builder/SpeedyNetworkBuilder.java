package io.kiw.speedy.builder;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.exception.ConflictingRouteException;

import java.util.*;

import static io.kiw.speedy.builder.BuilderValidation.validateWindowSize;

public class SpeedyNetworkBuilder {
    private final List<SpeedyChannel> speedyChannels = new ArrayList<>();
    private int defaultWindowSize = 65_536;

    public SpeedyNetwork buildNetwork() {
        return new SpeedyNetwork(speedyChannels, defaultWindowSize);
    }

    public SpeedyNetworkBuilder defaultWindowSize(int windowSize) {
        validateWindowSize(windowSize);

        this.defaultWindowSize = windowSize;
        return this;
    }

    public SpeedyNetworkBuilder addChannel(SpeedyChannel newRoute) {
        for (SpeedyChannel existingRoute : speedyChannels) {
            Set<SpeedyHost> duplicatePublishers = getDuplicates(newRoute.publisherHosts, existingRoute.publisherHosts);
            Set<SpeedyHost> duplicateSubscribers = getDuplicates(newRoute.subscriberHosts, existingRoute.subscriberHosts);
            Set<String> duplicateKeys = getDuplicates(newRoute.keys, existingRoute.keys);
            if(duplicatePublishers.size() > 0 && duplicateKeys.size() > 0 && duplicateSubscribers.size() > 0 )
            {
                throw new ConflictingRouteException("Route conflict found for new route:\n" + newRoute +
                        " \nagainst:\n" + existingRoute);
            }
        }

        speedyChannels.add(newRoute);
        return this;
    }

    private <T> Set<T> getDuplicates(Collection<T> collectionOne, Collection<T> collectionTwo)
    {
        HashSet<T> setOne = new HashSet<>(collectionOne);
        HashSet<T> setTwo = new HashSet<>(collectionTwo);

        HashSet<T> remainingSet = new HashSet<>(setOne);
        remainingSet.retainAll(setTwo);
        return remainingSet;
    }
}
