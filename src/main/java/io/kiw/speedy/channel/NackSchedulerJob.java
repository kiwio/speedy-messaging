package io.kiw.speedy.channel;

import io.kiw.speedy.helper.ImmutableLongMap;

public class NackSchedulerJob {
    private final ImmutableLongMap<NackTask> nackTasks;
    public NackSchedulerJob(ImmutableLongMap<NackTask> nackTasks) {

        this.nackTasks = nackTasks;
    }

    public void onNack(int channelIdentifier, int publisherIdentifier, long firstMissingSequenceNumber, long lastMissingSequenceNumber) {
        final long key = (long)channelIdentifier + publisherIdentifier;

        NackTask nackTask = nackTasks.get(key);
        if(nackTask != null)
        {
            nackTask.handleNack(firstMissingSequenceNumber, lastMissingSequenceNumber);
        }
    }


    public void onPulse(long nanoTime)
    {
        nackTasks.forEach(nt -> nt.onPulse(nanoTime));
    }
}
