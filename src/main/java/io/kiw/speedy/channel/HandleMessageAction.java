package io.kiw.speedy.channel;

public enum HandleMessageAction {
    HANDLE_MESSAGE_AND_CONTINUE_SCANNING,
    DROP_MESSAGE,
    SKIP_MESSAGE_BUT_CONTINUE_SCANNING
}
