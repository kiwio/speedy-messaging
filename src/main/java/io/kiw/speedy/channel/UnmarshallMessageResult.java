package io.kiw.speedy.channel;

import io.kiw.speedy.subscriber.SubscriberChannelState;

public class UnmarshallMessageResult {
    private HandleMessageAction handleMessageAction = null;
    private SubscriberChannelState sequenceState = null;

    public void clear() {
        handleMessageAction = null;
        sequenceState =null;
    }

    public HandleMessageAction getHandleMessageAction() {
        return handleMessageAction;
    }

    public SubscriberChannelState getSequenceState() {
        return sequenceState;
    }

    public void setResult(SubscriberChannelState subscriptionSequenceState, HandleMessageAction handleMessageAction) {
        sequenceState = subscriptionSequenceState;
        this.handleMessageAction = handleMessageAction;
    }
}
