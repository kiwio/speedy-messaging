package io.kiw.speedy.channel;

import io.kiw.speedy.management.ManagementKey;
import io.kiw.speedy.publisher.PublishPromise;

import java.util.concurrent.TimeUnit;

public class NackTask {

    public static final long NACK_TIMEOUT = TimeUnit.MILLISECONDS.toNanos(50);
    private final NackMarshaller nackMarshaller = new NackMarshaller();
    private final PublishPromise publishPromise;
    private final int publisherIdentifier;
    private final int channelIdentifier;
    private long lastSentNack = 0;
    private volatile boolean inProgress = false;
    private long firstMissingSequenceNumber;
    private long lastMissingSequenceNumber;

    public NackTask(PublishPromise publishPromise, int publisherIdentifier, int channelIdentifier) {

        this.publishPromise = publishPromise;
        this.publisherIdentifier = publisherIdentifier;
        this.channelIdentifier = channelIdentifier;
    }

    public void handleNack(long firstMissingSequenceNumber, long lastMissingSequenceNumber) {
        if(!inProgress)
        {
            this.firstMissingSequenceNumber = firstMissingSequenceNumber;
            this.lastMissingSequenceNumber = lastMissingSequenceNumber;
            inProgress = true;
        }
    }

    public void onPulse(long nanoTime) {
        if(inProgress && lastSentNack + NACK_TIMEOUT < nanoTime)
        {
            System.out.println("SUBSCRIBER: NACKED requesting resend on channel " + channelIdentifier + " for " +  firstMissingSequenceNumber + " to " + lastMissingSequenceNumber);
            lastSentNack = nanoTime;
            publishPromise.publish(ManagementKey.NACK.getKey(), nackMarshaller.marshall(publisherIdentifier, channelIdentifier, firstMissingSequenceNumber, lastMissingSequenceNumber));
            inProgress = false;
        }

    }
}
