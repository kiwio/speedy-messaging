package io.kiw.speedy.channel;

import java.nio.ByteBuffer;

public class NackMarshaller {
    private ByteBuffer marshallBuffer = ByteBuffer.allocate(44);

    public ByteBuffer marshall(int publisherIdentifier, int channelIdentifier, long firstPacketKeyMissing, long lastPacketKeyMissing) {
        marshallBuffer.clear();
        marshallBuffer.putInt(publisherIdentifier);
        marshallBuffer.putInt(channelIdentifier);
        marshallBuffer.putLong(firstPacketKeyMissing);
        marshallBuffer.putLong(lastPacketKeyMissing);
        marshallBuffer.flip();
        return marshallBuffer;
    }
}
