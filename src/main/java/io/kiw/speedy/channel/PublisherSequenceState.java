package io.kiw.speedy.channel;

import java.nio.ByteBuffer;

public class PublisherSequenceState {
    private final RecoveryBuffer recoveryBuffer;
    private final int windowSize;
    private final int localhostIdentifier;
    private final int channelIdentifier;
    private long packetSequenceNumber = 0;
    private long eventSequenceNumber = 0;

    private PublisherSequenceState(final int windowSize, int localhostIdentifier, int channelIdentifier) {
        this.recoveryBuffer = new RecoveryBuffer(windowSize);
        this.windowSize = windowSize;
        this.localhostIdentifier = localhostIdentifier;
        this.channelIdentifier = channelIdentifier;
    }

    public static PublisherSequenceState initialise(final int windowSize, int localhostIdentifier, int channelIdentifier)
    {
        PublisherSequenceState publisherSequenceState = new PublisherSequenceState(windowSize, localhostIdentifier, channelIdentifier);
        publisherSequenceState.preparePacket(publisherSequenceState.packetSequenceNumber);
        return publisherSequenceState;
    }



    public long getPacketSequenceNumber() {
        return packetSequenceNumber;
    }

    public ByteBuffer getMessage(long i) {
        return recoveryBuffer.getPacket(i);
    }

    public ByteBuffer recoverMessage(long index) {
        if(index > packetSequenceNumber - windowSize)
        {
            return recoveryBuffer.getPacket(index);
        }
        else
        {
            return null;
        }
    }



    public long getAndIncrementEventSequenceNumber() {
        return eventSequenceNumber++;
    }

    public ByteBuffer getCurrentPublisherBuffer() {
        return getMessage(packetSequenceNumber);
    }

    public void incrementAndMarshallNextPacketHeader() {
        preparePacket(++packetSequenceNumber);
    }

    private void preparePacket(final long packetSequenceNumber) {
        ByteBuffer packet = recoveryBuffer.getPacket(packetSequenceNumber);
        packet.clear();
        packet.putLong(packetSequenceNumber);
        packet.putInt(localhostIdentifier);
        packet.putInt(channelIdentifier);
    }
}
