package io.kiw.speedy.channel;

import java.nio.ByteBuffer;

import static io.kiw.speedy.SpeedyMessagingImpl.DATAGRAM_LENGTH;

public class RecoveryBuffer {
    private final ByteBuffer[] buffers;
    private final int limit;

    public RecoveryBuffer(int windowSize) {
        this.limit = windowSize - 1;
        buffers = createByteBuffers(windowSize);
    }

    public void addMessageToPacketRecoveryBuffer(long sequenceNumberOfPacket, ByteBuffer packetBuffer) {
        ByteBuffer bufferFromRecovery = buffers[getBufferIndex(sequenceNumberOfPacket)];
        bufferFromRecovery.clear();
        bufferFromRecovery.put(packetBuffer);
        bufferFromRecovery.flip();
    }

    public ByteBuffer getPacket(long sequenceNumber) {
        return buffers[getBufferIndex(sequenceNumber)];
    }

    public void markSeen(long sequenceNumberOfPacket) {
        ByteBuffer buffer = buffers[getBufferIndex(sequenceNumberOfPacket)];
        buffer.clear();
        buffer.position(DATAGRAM_LENGTH);
    }

    private int getBufferIndex(long sequenceNumberOfPacket) {
        return (int)(sequenceNumberOfPacket & limit);
    }

    private static ByteBuffer[] createByteBuffers(int windowSize) {
        ByteBuffer[] byteBuffers = new ByteBuffer[windowSize];
        for (int i = 0; i < windowSize; i++) {
            ByteBuffer allocate = ByteBuffer.allocateDirect(DATAGRAM_LENGTH);
            allocate.position(DATAGRAM_LENGTH); // simulate full / fully read
            byteBuffers[i] = allocate;

        }
        return byteBuffers;
    }
}
