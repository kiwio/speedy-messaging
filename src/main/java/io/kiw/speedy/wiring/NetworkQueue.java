package io.kiw.speedy.wiring;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

public class NetworkQueue {
    private final FragmentHandler fragmentHandler;
    private final Queue<byte[]> fragments = new ArrayBlockingQueue<>(10000);

    public NetworkQueue(FragmentHandler fragmentHandler) {

        this.fragmentHandler = fragmentHandler;
    }

    public void putFragment(byte[] bytes) {
        fragments.add(bytes);
    }

    public void executeFragments() {
        if(fragmentHandler != null)
        {
            while(fragments.size() > 0)
            {
                try {
                    fragmentHandler.handle();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }

    }

    public byte[] getFragment() {
        return fragments.poll();
    }

}
