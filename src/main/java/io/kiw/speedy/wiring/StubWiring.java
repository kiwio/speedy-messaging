package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.marshaller.OutboundDataHandler;
import io.kiw.speedy.marshaller.OutboundDataHandlerImpl;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.publisher.SpeedyMessagingPublisher;
import io.kiw.speedy.subscriber.HandleMessageEvent;
import io.kiw.speedy.subscriber.SpeedyMessagingSubscriber;
import io.kiw.speedy.wiring.thread.ThreadHandler;
import io.kiw.tetryon.Tetryon;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class StubWiring implements SpeedyWiring {
    private final SameThreadHandler sameThreadHandler = new SameThreadHandler();
    private StubRouter stubRouter;
    private SpeedyHost localHost;
    private OutboundDataHandlerImpl messageTranslator;
    private boolean autoFlushing = true;
    private List<Integer> eventWavesIds = new ArrayList<>();
    public StubWiring(StubRouter stubRouter, SpeedyHost localHost) {
        this.localHost = localHost;
        this.stubRouter = stubRouter;
    }

    @Override
    public void sendPacket(ByteBuffer writeBuffer, SpeedyHost remoteHost) {
        byte[] bytes = new byte[writeBuffer.remaining()];
        writeBuffer.get(bytes, writeBuffer.position(), writeBuffer.limit());
        stubRouter.send(bytes, remoteHost, getCurrentEventId());

    }

    public Integer getCurrentEventId() {
        return eventWavesIds.get(eventWavesIds.size() -1);
    }

    @Override
    public void closePublisher() {

    }

    @Override
    public void receive(ByteBuffer readBuffer) {
        readBuffer.put(stubRouter.receive(localHost));
    }

    @Override
    public int receive(ByteBuffer[] readBuffer) throws IOException {
        readBuffer[0].put(stubRouter.receive(localHost));
        return 1;
    }

    @Override
    public void closeSubscriber() {

    }

    @Override
    public void registerFragmentHandler(FragmentHandler fragmentHandler) {
        stubRouter.addFragmentHandler(localHost, fragmentHandler);
    }

    @Override
    public ThreadHandler getThreadHandler() {
        return sameThreadHandler;
    }

    @Override
    public void connectToRemoteHost(SpeedyConnection speedyConnection, Runnable publishTask) {
        publishTask.run();
    }

    @Override
    public void start(SpeedyMessagingPublisher speedyMessagingPublisher, SpeedyMessagingSubscriber speedyMessagingSubscriber, SchedulerThread schedulerThread, ImmutableIntMap<SpeedyConnection> remoteConnections) {
        speedyMessagingPublisher.enable();
        speedyMessagingSubscriber.addNackScheduledJob();
    }

    @Override
    public void connectIfSingleConnection(ImmutableIntMap<SpeedyConnection> remoteConnections) {

    }

    @Override
    public int startEvent() {
        int eventWaveId = stubRouter.startEvent();
        eventWavesIds.add(eventWaveId);
        return eventWaveId;
    }

    @Override
    public void addPulseHandler(Runnable pulseHandler) {
        stubRouter.addPulseHandler(pulseHandler);
    }

    @Override
    public MultiThreadSubscriberHandler buildMultiThreadSubscriberHandler(List<Tetryon.EventHandler<HandleMessageEvent>> group) {
        return new StubMultiThreadSubscriberHandler(group);
    }

    @Override
    public void completeEvent(int eventId) {
        if(autoFlushing)
        {
            messageTranslator.flushDataIfRequired();
        }
        eventWavesIds.remove(eventWavesIds.size() - 1);
        stubRouter.completeEvent(eventId);
    }

    @Override
    public long getNanoTime() {
        return stubRouter.getNanoTime();
    }

    @Override
    public OutboundDataHandler wrapPacketHandler(OutboundDataHandlerImpl delegate) {
        messageTranslator = delegate;
        return messageTranslator;
    }

    public void setAutoFlushing(boolean autoFlushing) {
        this.autoFlushing = autoFlushing;
    }
}
