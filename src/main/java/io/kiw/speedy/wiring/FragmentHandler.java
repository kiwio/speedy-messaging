package io.kiw.speedy.wiring;

import java.io.IOException;

public interface FragmentHandler {
    void handle() throws IOException;
}
