package io.kiw.speedy.wiring;

import io.kiw.speedy.wiring.thread.NoOpSleepHandler;
import io.kiw.speedy.wiring.thread.SleepableTask;
import io.kiw.speedy.wiring.thread.ThreadHandler;

public class SameThreadHandler implements ThreadHandler {

    @Override
    public void run(SleepableTask runnable) {
        runnable.run(new NoOpSleepHandler());
    }

    @Override
    public void join() {

    }
}
