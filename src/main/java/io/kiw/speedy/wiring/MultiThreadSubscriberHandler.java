package io.kiw.speedy.wiring;

import io.kiw.speedy.subscriber.HandleMessageEvent;

public interface MultiThreadSubscriberHandler {
    void handleMessage(HandleMessageEvent event);

    void close();
}
