package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.marshaller.OutboundDataHandler;
import io.kiw.speedy.marshaller.OutboundDataHandlerImpl;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.publisher.SpeedyMessagingPublisher;
import io.kiw.speedy.subscriber.HandleMessageEvent;
import io.kiw.speedy.subscriber.SpeedyMessagingSubscriber;
import io.kiw.speedy.wiring.thread.ThreadHandler;
import io.kiw.tetryon.Tetryon;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.List;

public interface SpeedyWiring {
    void sendPacket(ByteBuffer writeBuffer, SpeedyHost socketAddress);

    void closePublisher();

    void receive(ByteBuffer readBuffer) throws IOException;
    int receive(ByteBuffer[] readBuffer) throws IOException;

    void closeSubscriber();

    void registerFragmentHandler(FragmentHandler fragmentHandler);

    ThreadHandler getThreadHandler();

    void connectToRemoteHost(SpeedyConnection speedyConnection, Runnable publishTask);

    void start(SpeedyMessagingPublisher speedyMessagingPublisher, SpeedyMessagingSubscriber speedyMessagingSubscriber, SchedulerThread schedulerThread, ImmutableIntMap<SpeedyConnection> remoteConnections);

    void connectIfSingleConnection(ImmutableIntMap<SpeedyConnection> remoteConnections);

    void completeEvent(int eventId);

    long getNanoTime();

    OutboundDataHandler wrapPacketHandler(OutboundDataHandlerImpl delegate);

    int startEvent();

    void addPulseHandler(Runnable pulseHandler);

    MultiThreadSubscriberHandler buildMultiThreadSubscriberHandler(List<Tetryon.EventHandler<HandleMessageEvent>> group);

}
