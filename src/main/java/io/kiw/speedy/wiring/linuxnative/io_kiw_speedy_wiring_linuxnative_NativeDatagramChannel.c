#include <jni.h>
#include <stdio.h>
#include "io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel.h"
#include<arpa/inet.h>
#include<sys/socket.h>
#include<string.h> //memset
#include<stdbool.h> //boolean
#include<stdlib.h> //exit(0);
#include <errno.h>
#define BUFLEN 5920  //Max length of buffer
#define BATCHSIZE 10  //Max length of buffer
#define PORT 8888   //The port on which to listen for incoming data

void die(char *s)
{
    perror(s);
    exit(1);
}

struct mmsghdr {
	struct msghdr	msg_hdr;
	unsigned int msg_len;
};


JNIEXPORT jint JNICALL Java_io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel_openSocket(JNIEnv *env, jclass object, jint port)
{
    struct sockaddr_in socket_details;

    int s, i , recv_len;

    //create a UDP socket
    if ((s=socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
    {
         die("socket");
    }

    if(setsockopt(s, SOL_SOCKET, SO_REUSEADDR, &(int){ 1 }, sizeof(int)) < 0)
    {
         die("socket-options: SO_REUSEADDR");
    }
    int rcv_buffer_size = 16777216;
    if ( (setsockopt(s, SOL_SOCKET, SO_RCVBUF, &rcv_buffer_size, sizeof(int)) ) < 0 )
    {
        die("socket-options: SO_RCVBUF");
    }

    // zero out the structure
    memset((char *) &socket_details, 0, sizeof(socket_details));

    socket_details.sin_family = AF_INET;
    if(port != -1)
    {
        socket_details.sin_port = htons(port);
      //bind socket to port
        if( bind(s, (struct sockaddr*)&socket_details, sizeof(socket_details) ) == -1)
        {
            die("bind");
        }
    }
    socket_details.sin_addr.s_addr = htonl(INADDR_ANY);



   return s;
}

JNIEXPORT jbyte JNICALL Java_io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel_sendTo(JNIEnv *env, jobject channel, jobject byteBuffer, jint socket_descriptor, jint destination_address, jint destination_port, jint buffer_size)
{
    struct sockaddr_in outbound_address;
    memset((char *) &outbound_address, 0, sizeof(outbound_address));

    int slen = sizeof(outbound_address);
    jbyte *direct_buffer = (*env)->GetDirectBufferAddress(env, byteBuffer);

    memset((char*)&outbound_address, 0, sizeof(outbound_address));
    outbound_address.sin_family = AF_INET;
    outbound_address.sin_port = htons(destination_port);
    outbound_address.sin_addr.s_addr = destination_address;
    if (sendto(socket_descriptor, direct_buffer, buffer_size , 0 , (struct sockaddr *) &outbound_address, slen) ==-1)
    {
        die("sendto()");
    }


    return direct_buffer[0];
}

JNIEXPORT jint JNICALL Java_io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel_receiveFrom(JNIEnv *env, jobject channel, jobject byteBuffer, jint socket_descriptor)
{
    jbyte *direct_buffer = (*env)->GetDirectBufferAddress(env, byteBuffer);
    return recvfrom(socket_descriptor, direct_buffer, BUFLEN, 0, NULL, NULL);
}


JNIEXPORT void JNICALL Java_io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel_closeSocket(JNIEnv *env, jobject channel, jint socket_descriptor)
{
    if(close(socket_descriptor) == -1)
    {
        die("close()");
    }
}

JNIEXPORT jint JNICALL Java_io_kiw_speedy_wiring_linuxnative_NativeDatagramChannel_receiveMessages(JNIEnv *env, jobject channel, jobjectArray direct_buffers, jint socket_descriptor, jintArray read_bytes_array)
{
    struct iovec iovec[BATCHSIZE][1];
    struct mmsghdr datagrams[BATCHSIZE];
    struct sockaddr addr[BATCHSIZE];
    jint *elms = (*env)->GetIntArrayElements(env, read_bytes_array, 0);
    for(int i = 0; i < BATCHSIZE; i++)
    {
        jbyte* *direct_buffer = (*env)->GetDirectBufferAddress(env, (*env)->GetObjectArrayElement(env, direct_buffers, i));

        iovec[i][0].iov_base = direct_buffer;
        iovec[i][0].iov_len  = BUFLEN;
        datagrams[i].msg_hdr.msg_iov	 = iovec[i];
        datagrams[i].msg_hdr.msg_iovlen	 = 1;
        datagrams[i].msg_hdr.msg_name	 = &addr[i];
        datagrams[i].msg_hdr.msg_namelen = sizeof(addr[i]);
    }

    int nr_datagrams = recvmmsg(socket_descriptor, datagrams, BATCHSIZE, MSG_WAITFORONE, 0);

    if(nr_datagrams == -1)
    {
        printf("%i ", errno);
        die("Multi receive");
    }

    for(int i = 0; i < nr_datagrams; i++)
    {
        int read = datagrams[i].msg_len;
        elms[i] = read;
    }

    (*env)->SetIntArrayRegion(env, read_bytes_array, 0, BATCHSIZE, elms);
    (*env)->ReleaseIntArrayElements(env, read_bytes_array, elms, 0);
    return nr_datagrams;
}