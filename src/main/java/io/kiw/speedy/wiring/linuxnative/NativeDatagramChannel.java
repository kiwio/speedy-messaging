package io.kiw.speedy.wiring.linuxnative;

import io.kiw.speedy.helper.PacketCat;

import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;

public class NativeDatagramChannel {

    public static final int BATCH_SIZE = 10;
    int[] bytesRead = new int[BATCH_SIZE];
    static {
        System.loadLibrary("linuxdatachannel");
    }

    private final int socketDescriptor;

    private NativeDatagramChannel(int socketDescriptor) {
        this.socketDescriptor = socketDescriptor;
    }

    public static NativeDatagramChannel open() {
        int socketDescriptor = openSocket(-1);

        NativeDatagramChannel nativeDatagramChannel = new NativeDatagramChannel(socketDescriptor);

        return nativeDatagramChannel;
    }

    public static NativeDatagramChannel open(int port) {
        int socketDescriptor = openSocket(port);

        NativeDatagramChannel nativeDatagramChannel = new NativeDatagramChannel(socketDescriptor);

        return nativeDatagramChannel;
    }

    public void send(ByteBuffer byteBuffer, int destinationPort, int ipAddress)
    {
        sendTo(byteBuffer, socketDescriptor, ipAddress, destinationPort, byteBuffer.limit());
        byteBuffer.position(byteBuffer.limit());
    }


    public void receive(ByteBuffer byteBuffer) throws ClosedByInterruptException {
        int bytesReceived;
        bytesReceived = receiveFrom(byteBuffer, socketDescriptor);

        if(bytesReceived == -1)
        {
            throw new ClosedByInterruptException();
        }
        byteBuffer.limit(bytesReceived);
        byteBuffer.position(bytesReceived);
    }

    public int receiveMessages(ByteBuffer[] byteBuffers) throws ClosedByInterruptException {
        resetIndicies();

        int packetsRead = receiveMessages(byteBuffers, socketDescriptor, bytesRead);

        if(packetsRead == -1)
        {
            throw new ClosedByInterruptException();
        }

        for (int i = 0; i < packetsRead; i++) {
            byteBuffers[i].limit(bytesRead[i]);
            byteBuffers[i].position(bytesRead[i]);
        }

        return packetsRead;
    }

    private void resetIndicies() {
        for (int i = 0; i < bytesRead.length; i++) {
            bytesRead[i] = 0;
        }
    }

    private static native int openSocket(int port);

    public native byte sendTo(ByteBuffer byteBuffer, int socketDescriptor, int ipAddress, int destinationPort, int limit);

    private native int receiveFrom(ByteBuffer byteBuffer, int socketDescriptor);

    private native int receiveMessages(ByteBuffer[] byteBuffer, int socketDescriptor, int[] bytesRead);


    public void close() {
        closeSocket(socketDescriptor);
    }

    private native void closeSocket(int socketDescriptor);
}
