package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyHost;

public class EventResponse {

    final byte[] bytes;
    final SpeedyHost remoteHost;
    final int eventId;

    public EventResponse(byte[] bytes, SpeedyHost remoteHost, int eventId) {
        this.bytes = bytes;
        this.remoteHost = remoteHost;
        this.eventId = eventId;
    }

}
