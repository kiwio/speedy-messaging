package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.marshaller.OutboundDataHandler;
import io.kiw.speedy.marshaller.OutboundDataHandlerImpl;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.publisher.SpeedyMessagingPublisher;
import io.kiw.speedy.subscriber.HandleMessageEvent;
import io.kiw.speedy.subscriber.SpeedyMessagingSubscriber;
import io.kiw.speedy.wiring.linuxnative.NativeDatagramChannel;
import io.kiw.speedy.wiring.thread.MultiThreadHandler;
import io.kiw.speedy.wiring.thread.ThreadHandler;
import io.kiw.tetryon.Tetryon;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedByInterruptException;
import java.util.List;


public class LinuxUdpSpeedyWiring implements SpeedyWiring {

    private final NativeDatagramChannel subscriberDatagramChannel;
    private final NativeDatagramChannel publisherDatagramChannel;
    private final MultiThreadHandler multiThreadHandler = new MultiThreadHandler();
    private Thread subcriberThread;
    public LinuxUdpSpeedyWiring(int subscriberPort) {

        this.subscriberDatagramChannel =  createsubscriberDatagramChannel(subscriberPort);
        this.publisherDatagramChannel = createPublisherDatagramChannel();
    }

    @Override
    public void sendPacket(ByteBuffer writeBuffer, SpeedyHost speedyHost) {
        publisherDatagramChannel.send(writeBuffer, speedyHost.getPort(), speedyHost.getIpAddressAsInt());
    }

    @Override
    public void closePublisher() {
        publisherDatagramChannel.close();
    }

    @Override
    public void receive(ByteBuffer readBuffer) throws ClosedByInterruptException {
        subscriberDatagramChannel.receive(readBuffer);
    }

    @Override
    public int receive(ByteBuffer[] readBuffers) throws IOException {
        return subscriberDatagramChannel.receiveMessages(readBuffers);
    }

    @Override
    public void closeSubscriber() {
        subscriberDatagramChannel.close();
        subcriberThread.interrupt();
    }

    @Override
    public void registerFragmentHandler(FragmentHandler fragmentHandler) {
        // do nothing
    }

    @Override
    public ThreadHandler getThreadHandler() {
        return multiThreadHandler;
    }

    @Override
    public void connectToRemoteHost(SpeedyConnection speedyConnection, Runnable publishTask) {
        getThreadHandler().run((sleepHandler) -> {
            while(!speedyConnection.theyHaveAcknowledgedUs())
            {
                publishTask.run();
                sleepHandler.sleep(10);
            }

            while(!speedyConnection.weHaveAcknowledgedThem())
            {
                sleepHandler.sleep(10);
            }
        });

    }

    @Override
    public void start(SpeedyMessagingPublisher speedyMessagingPublisher, SpeedyMessagingSubscriber speedyMessagingSubscriber, SchedulerThread schedulerThread, ImmutableIntMap<SpeedyConnection> remoteConnections) {
        speedyMessagingSubscriber.addNackScheduledJob();
        speedyMessagingPublisher.start();
        subcriberThread = new Thread(speedyMessagingSubscriber, "SpeedyMessagingSubscriber-Thread");
        subcriberThread.start();
        schedulerThread.start();

    }

    @Override
    public void connectIfSingleConnection(ImmutableIntMap<SpeedyConnection> remoteConnections) {

    }

    @Override
    public void completeEvent(int eventId) {

    }

    @Override
    public long getNanoTime() {
        return System.nanoTime();
    }

    @Override
    public OutboundDataHandler wrapPacketHandler(OutboundDataHandlerImpl packetHandler) {
        return packetHandler;
    }

    @Override
    public int startEvent() {
        return 0;
    }

    @Override
    public void addPulseHandler(Runnable pulse) {

    }

    @Override
    public MultiThreadSubscriberHandler buildMultiThreadSubscriberHandler(List<Tetryon.EventHandler<HandleMessageEvent>> group) {
        return new TetryonMultiThreadSubscriberHandler(group);
    }

    private static NativeDatagramChannel createsubscriberDatagramChannel(int port) {
        return NativeDatagramChannel.open(port);
    }

    private static NativeDatagramChannel createPublisherDatagramChannel() {
        return NativeDatagramChannel.open();
    }
}
