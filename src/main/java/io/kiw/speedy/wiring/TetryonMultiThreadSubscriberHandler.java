package io.kiw.speedy.wiring;

import io.kiw.speedy.subscriber.HandleMessageEvent;
import io.kiw.tetryon.SinglePublisherMode;
import io.kiw.tetryon.Tetryon;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class TetryonMultiThreadSubscriberHandler implements MultiThreadSubscriberHandler {

    private final Tetryon<HandleMessageEvent> handleMessageEventTetryon;

    public TetryonMultiThreadSubscriberHandler(List<Tetryon.EventHandler<HandleMessageEvent>> group) {
        this.handleMessageEventTetryon = Tetryon.buildInstance(32, HandleMessageEvent::new, HandleMessageEvent::overwrite, new SinglePublisherMode<>(), group, TimeUnit.MICROSECONDS.toNanos(100));
        handleMessageEventTetryon.start();
    }


    @Override
    public void handleMessage(HandleMessageEvent event)
    {
        handleMessageEventTetryon.publishEvent(event);
    }

    @Override
    public void close()
    {
        handleMessageEventTetryon.shutdown();
    }
}
