package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyHost;

import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;

public class StubRouter {

    private int eventId = 0;
    private int completedEventId = 0;
    private boolean started = false;
    private final Map<SpeedyHost, NetworkQueue> networkQueues = new HashMap<>();
    private final List<EventResponse> eventResponseList = new ArrayList<>();
    private final Queue<EventResponse> preStartMessages = new ArrayBlockingQueue<>(1024);
    private final List<Runnable> pulseHandlers = new ArrayList<>();
    private boolean droppingPackets = false;
    private long nanoTime = System.nanoTime();

    public void addFragmentHandler(SpeedyHost localHost, FragmentHandler fragmentHandler) {
        networkQueues.put(localHost, new NetworkQueue(fragmentHandler));
    }

    public byte[] receive(SpeedyHost localHost) {
        return networkQueues.get(localHost).getFragment();
    }

    public void send(byte[] bytes, SpeedyHost remoteHost, int eventId) {
        if(started)
        {
            if(this.eventId == eventId)
            {
                if(!droppingPackets)
                {
                    NetworkQueue networkQueue = networkQueues.get(remoteHost);
                    networkQueue.putFragment(bytes);
                    networkQueue.executeFragments();
                }

            }
            else
            {
                eventResponseList.add(new EventResponse(bytes, remoteHost, eventId));
            }
        }
        else
        {
            preStartMessages.add(new EventResponse(bytes, remoteHost, eventId));
        }
    }

    public void start() {
        started = true;
        EventResponse preStartMessage;
        int eventId = startEvent();
        while((preStartMessage = preStartMessages.poll()) != null)
        {

            send(preStartMessage.bytes, preStartMessage.remoteHost, eventId);
        }
        completeEvent(eventId);
    }

    public void startDroppingPackets() {
        droppingPackets = true;
    }

    public void stopDroppingPackets() {
        droppingPackets = false;
    }

    public int startEvent() {
        int currentEventId = this.eventId;
        if(started && currentEventId == completedEventId)
        {
            return ++this.eventId;
        }
        return -1;

    }

    public void completeEvent(int eventId) {
        if(started)
        {
            if(eventId == completedEventId + 1)
            {
                completedEventId++;

                if(!eventResponseList.isEmpty())
                {
                    int newEventId = startEvent();
                    List<EventResponse> tempCopy = new ArrayList<>(eventResponseList);
                    eventResponseList.clear();
                    for (EventResponse eventResponse : tempCopy) {
                        send(eventResponse.bytes, eventResponse.remoteHost, newEventId);
                    }

                    completeEvent(newEventId);
                }

            }
        }
    }

    public void onPulse()
    {
        pulseHandlers.forEach(Runnable::run);
    }

    public void fastForwardTimeInNanos(long timeInNanosToFastForwardBy){
        nanoTime += timeInNanosToFastForwardBy;
    }

    public long getNanoTime() {
        return nanoTime;
    }

    public void addPulseHandler(Runnable pulseHandler) {
        pulseHandlers.add(pulseHandler);
    }
}
