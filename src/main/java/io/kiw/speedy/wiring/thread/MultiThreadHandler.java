package io.kiw.speedy.wiring.thread;

import java.util.ArrayList;
import java.util.List;

public class MultiThreadHandler implements ThreadHandler{

    final List<Thread> threads = new ArrayList<>();
    final SleepHandler sleepHandler = new RealSleepHandler();
    @Override
    public void run(SleepableTask sleepableTask) {

        Thread thread = new Thread(() -> sleepableTask.run(sleepHandler));
        thread.start();
        threads.add(thread);
    }

    @Override
    public void join() {
        threads.forEach(t -> {
            try {
                t.join();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public void interrupt() {
        threads.forEach(Thread::interrupt);
    }
}
