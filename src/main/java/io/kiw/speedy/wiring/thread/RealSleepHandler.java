package io.kiw.speedy.wiring.thread;

public class RealSleepHandler implements SleepHandler {
    @Override
    public void sleep(int i) {
        try {
            Thread.sleep(i);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
