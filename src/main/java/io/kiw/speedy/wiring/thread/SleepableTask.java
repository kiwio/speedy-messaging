package io.kiw.speedy.wiring.thread;

public interface SleepableTask {
    void run(SleepHandler sleepHandler);
}