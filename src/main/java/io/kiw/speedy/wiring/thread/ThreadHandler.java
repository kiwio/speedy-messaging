package io.kiw.speedy.wiring.thread;

public interface ThreadHandler {
     void run(SleepableTask runnable);

     void join();
}
