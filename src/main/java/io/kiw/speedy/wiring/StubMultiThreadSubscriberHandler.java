package io.kiw.speedy.wiring;

import io.kiw.speedy.subscriber.HandleMessageEvent;
import io.kiw.tetryon.Tetryon;

import java.util.List;

public class StubMultiThreadSubscriberHandler implements MultiThreadSubscriberHandler {
    private final List<Tetryon.EventHandler<HandleMessageEvent>> group;

    public StubMultiThreadSubscriberHandler(List<Tetryon.EventHandler<HandleMessageEvent>> group) {
        this.group = group;
    }

    @Override
    public void handleMessage(HandleMessageEvent event) {
        for (Tetryon.EventHandler<HandleMessageEvent> handleMessageEventEventHandler : group) {
            handleMessageEventEventHandler.handle(event);
        }

    }

    @Override
    public void close() {

    }
}
