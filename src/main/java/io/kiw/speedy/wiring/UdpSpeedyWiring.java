package io.kiw.speedy.wiring;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.marshaller.OutboundDataHandler;
import io.kiw.speedy.marshaller.OutboundDataHandlerImpl;
import io.kiw.speedy.publisher.SchedulerThread;
import io.kiw.speedy.publisher.SpeedyMessagingPublisher;
import io.kiw.speedy.subscriber.HandleMessageEvent;
import io.kiw.speedy.subscriber.SpeedyMessagingSubscriber;
import io.kiw.speedy.wiring.thread.MultiThreadHandler;
import io.kiw.speedy.wiring.thread.ThreadHandler;
import io.kiw.tetryon.Tetryon;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.ClosedChannelException;
import java.nio.channels.DatagramChannel;
import java.util.List;


public class UdpSpeedyWiring implements SpeedyWiring {

    public static final int RCF_BUFFER_SIZE = 16_777_216;

    private final DatagramChannel subscriberDatagramChannel;
    private final DatagramChannel publisherDatagramChannel;
    private final MultiThreadHandler multiThreadHandler = new MultiThreadHandler();
    private Thread subcriberThread;
    public UdpSpeedyWiring(int subscriberPort) {

        this.subscriberDatagramChannel =  createsubscriberDatagramChannel(subscriberPort);
        this.publisherDatagramChannel = createPublisherDatagramChannel();
    }

    @Override
    public void sendPacket(ByteBuffer writeBuffer, SpeedyHost speedyHost) {
        try {
            publisherDatagramChannel.send(writeBuffer, speedyHost.getSocketAddress());
        }
        catch (ClosedChannelException e) {
            // ignore
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void closePublisher() {
        try {
            publisherDatagramChannel.close();
        } catch (IOException e) {
            // ignore
        }
    }

    @Override
    public void receive(ByteBuffer readBuffer) throws IOException {
        subscriberDatagramChannel.receive(readBuffer);
    }

    @Override
    public int receive(ByteBuffer[] readBuffers) throws IOException {

        subscriberDatagramChannel.receive(readBuffers[0]);
        return 1;
    }

    @Override
    public void closeSubscriber() {
        try {
            subscriberDatagramChannel.close();
        } catch (IOException e) {
            // ignore
        }
        subcriberThread.interrupt();
    }

    @Override
    public void registerFragmentHandler(FragmentHandler fragmentHandler)  {
        // do nothing
    }

    @Override
    public ThreadHandler getThreadHandler() {
        return multiThreadHandler;
    }

    @Override
    public void connectToRemoteHost(SpeedyConnection speedyConnection, Runnable publishTask) {
        getThreadHandler().run((sleepHandler) -> {
            while(!speedyConnection.theyHaveAcknowledgedUs())
            {
                publishTask.run();
                sleepHandler.sleep(10);
            }

            while(!speedyConnection.weHaveAcknowledgedThem())
            {
                sleepHandler.sleep(10);
            }
        });

    }

    @Override
    public void start(SpeedyMessagingPublisher speedyMessagingPublisher, SpeedyMessagingSubscriber speedyMessagingSubscriber, SchedulerThread schedulerThread, ImmutableIntMap<SpeedyConnection> remoteConnections) {
        speedyMessagingSubscriber.addNackScheduledJob();
        speedyMessagingPublisher.start();
        subcriberThread = new Thread(speedyMessagingSubscriber, "SpeedyMessagingSubscriber-Thread");
        subcriberThread.start();
        schedulerThread.start();

    }

    @Override
    public void connectIfSingleConnection(ImmutableIntMap<SpeedyConnection> remoteConnections) {
        if(remoteConnections.size() == 1)
        {
            try {
                publisherDatagramChannel.connect(remoteConnections.values().iterator().next().getHost().getSocketAddress());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public void completeEvent(int eventId) {

    }

    @Override
    public long getNanoTime() {
        return System.nanoTime();
    }

    @Override
    public OutboundDataHandler wrapPacketHandler(OutboundDataHandlerImpl packetHandler) {
        return packetHandler;
    }

    @Override
    public int startEvent() {
        return 0;
    }

    @Override
    public void addPulseHandler(Runnable pulse) {

    }

    @Override
    public MultiThreadSubscriberHandler buildMultiThreadSubscriberHandler(List<Tetryon.EventHandler<HandleMessageEvent>> group) {
        return new TetryonMultiThreadSubscriberHandler(group);
    }

    private static DatagramChannel createsubscriberDatagramChannel(int port) {
        try {
            return DatagramChannel.open(StandardProtocolFamily.INET).setOption(StandardSocketOptions.SO_RCVBUF, RCF_BUFFER_SIZE).setOption(StandardSocketOptions.SO_REUSEADDR, true)
                    .bind(new InetSocketAddress(port));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static DatagramChannel createPublisherDatagramChannel() {
        try {
            return DatagramChannel.open(StandardProtocolFamily.INET);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
