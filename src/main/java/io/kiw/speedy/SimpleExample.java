package io.kiw.speedy;

import io.kiw.speedy.builder.SpeedyChannel;
import io.kiw.speedy.builder.SpeedyNetwork;
import io.kiw.speedy.builder.SpeedyNetworkBuilder;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import static io.kiw.speedy.parallel.ParallelHelper.parallel;

public class SimpleExample {

    private static final String EVENT_KEY = "eventKey";
    private static final String CHANNEL_NAME = "simplePubSub";

    public static void main(String[] args) throws InterruptedException, IOException {

        ExampleNetwork exampleNetwork = new ExampleNetwork();

        SpeedyMessagingImpl publisher = exampleNetwork.buildPublisher();
        SpeedyMessagingImpl subscriber = exampleNetwork.buildSubscriber();

        ExampleSubscriber.addSubscriptionHandler(subscriber);

        // start messengers in different threads as they block until they detect each other
        parallel(subscriber::start, publisher::start);

        ExamplePublisher.sendMessages(publisher);

        publisher.close();
        subscriber.close();
    }

    private static class ExampleNetwork {

        private final SpeedyHost publisherHost = new SpeedyHost("publisher", "127.0.0.1", 8543);
        private SpeedyHost susbcriberHost = new SpeedyHost("subscriber", "127.0.0.1", 8544);
        public SpeedyNetwork exampleNetwork = new SpeedyNetworkBuilder()
                .addChannel(
                        SpeedyChannel.newChannel(CHANNEL_NAME)
                        .withPublishers(publisherHost)
                        .forKeys(EVENT_KEY)
                        .toSubscribers(susbcriberHost))
                .buildNetwork();

        public SpeedyMessagingImpl buildPublisher() {
            return exampleNetwork.createInstanceBuilderFromPerspective(publisherHost).build();
        }

        public SpeedyMessagingImpl buildSubscriber() {
            return exampleNetwork.createInstanceBuilderFromPerspective(susbcriberHost).build();
        }
    }

    private static class ExampleSubscriber {
        public static void addSubscriptionHandler(final SpeedyMessagingImpl subscriber) {
            subscriber.subscribe(EVENT_KEY, message -> {

                byte[] bytes = new byte[message.remaining()];
                message.get(bytes);
                System.out.println(new String(bytes));
            });
        }
    }

    private static class ExamplePublisher {

        public static void sendMessages(SpeedyMessagingImpl publisher) {
            final ByteBuffer  buffer = ByteBuffer.allocate(1000);


            for (int i = 0; i < 10; i++) {
                buffer.clear();
                buffer.put(("hello, this is message " + i).getBytes(Charset.forName("UTF-8")));
                publisher.publish(EVENT_KEY, buffer);
            }
        }
    }
}
