package io.kiw.speedy.management;

public class NackMessage {
    public final int originalPublisherIdentifier;
    public final int hostRequiringRecoveryIdentifier;
    public int keyIdentifier;
    public final long firstPacketKeyMissing;
    public final long lastPacketKeyMissing;

    public NackMessage(int originalPublisherIdentifier, int hostRequiringRecoveryIdentifier, int keyIdentifier, long firstPacketKeyMissing, long lastPacketKeyMissing) {

        this.originalPublisherIdentifier = originalPublisherIdentifier;
        this.hostRequiringRecoveryIdentifier = hostRequiringRecoveryIdentifier;
        this.keyIdentifier = keyIdentifier;
        this.firstPacketKeyMissing = firstPacketKeyMissing;
        this.lastPacketKeyMissing = lastPacketKeyMissing;
    }
}
