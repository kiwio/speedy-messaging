package io.kiw.speedy.management;

import io.kiw.speedy.SpeedyHost;
import io.kiw.speedy.marshaller.ByteSizes;

import java.nio.ByteBuffer;

public class HostRegistrationMessage {
    private final SpeedyHost senderHost;
    private final SpeedyHost receiverHost;

    public HostRegistrationMessage(SpeedyHost senderHost, SpeedyHost receiverHost) {
        this.senderHost = senderHost;
        this.receiverHost = receiverHost;
    }

    public ByteBuffer marshall() {
        byte[] senderHostMarshalled = senderHost.marshalled();
        byte[] receiverHostMarshalled = receiverHost.marshalled();
        ByteBuffer buffer =  ByteBuffer.allocate(senderHostMarshalled.length + receiverHostMarshalled.length + (ByteSizes.INTEGER * 2))
                .putInt(senderHostMarshalled.length)
                .putInt(receiverHostMarshalled.length)
                .put(senderHostMarshalled)
                .put(receiverHostMarshalled);

        buffer.flip();
        return buffer;
    }

    public static HostRegistrationMessage unmarshall(ByteBuffer message) {
        int senderHostLength = message.getInt();
        int receiverHostLength = message.getInt();

        byte[] senderHostBytes = new byte[senderHostLength];
        message.get(senderHostBytes, 0, senderHostLength);
        SpeedyHost senderHost = SpeedyHost.unmarshall(senderHostBytes);

        byte[] receiverHostBytes = new byte[receiverHostLength];
        message.get(receiverHostBytes, 0, receiverHostLength);
        SpeedyHost receiverHost = SpeedyHost.unmarshall(receiverHostBytes);

        return new HostRegistrationMessage(senderHost, receiverHost);
    }

    public SpeedyHost getSenderHost() {
        return senderHost;
    }

    public SpeedyHost getReceiverHost() {
        return receiverHost;
    }
}
