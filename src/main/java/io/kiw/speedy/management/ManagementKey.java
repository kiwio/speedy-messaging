package io.kiw.speedy.management;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;


public enum ManagementKey {
    HOST_REGISTRATION("HOST_REGISTRATION"),
    HOST_ACKNOWLEDGEMENT("HOST_ACKNOWLEDGEMENT"),
    NACK("NACK");

    public static final ManagementKey[] MANAGEMENT_KEYS = values();
    public static final Set<String> MANAGEMENT_KEY_SET = Arrays.stream(MANAGEMENT_KEYS).map(ManagementKey::getKey).collect(Collectors.toSet());
    private final String managementRouteKey;

    ManagementKey(String managementRouteKey) {
        this.managementRouteKey = managementRouteKey;
    }

    public String getKey() {
        return managementRouteKey;
    }

    public static boolean isManagementKey(String key) {
        return MANAGEMENT_KEY_SET.contains(key);
    }
}
