package io.kiw.speedy;

import io.kiw.speedy.exception.SpeedyMessagingInitiationException;
import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.subscriber.SubscriberChannelState;

import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class SpeedyConnection {
    private final SpeedyHost remoteHost;
    private final AtomicBoolean theyHaveAcknowledgedUs = new AtomicBoolean(false);
    private final AtomicBoolean weHaveAcknowledgedThem = new AtomicBoolean(false);
    private final Set<Integer> keyIdentifiers;
    private final ImmutableIntMap<SubscriberChannelState> channelSequenceStates;

    public SpeedyConnection(SpeedyHost remoteHost, Set<Integer> keyIdentifiers, ImmutableIntMap<SubscriberChannelState> channelSequenceStates) {
        this.remoteHost = remoteHost;
        this.keyIdentifiers = keyIdentifiers;
        this.channelSequenceStates = channelSequenceStates;
    }

    public SpeedyHost getHost() {
        return remoteHost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpeedyConnection that = (SpeedyConnection) o;

        return remoteHost != null ? remoteHost.equals(that.remoteHost) : that.remoteHost == null;

    }

    @Override
    public int hashCode() {
        return remoteHost != null ? remoteHost.hashCode() : 0;
    }

    public boolean theyHaveAcknowledgedUs() {
        return theyHaveAcknowledgedUs.get();
    }

    public void theyAcknowledgeUs() {
        theyHaveAcknowledgedUs.set(true);
    }

    public void weAcknowledgeThem() {
        weHaveAcknowledgedThem.set(true);
    }

    public boolean weHaveAcknowledgedThem() {
        return weHaveAcknowledgedThem.get();
    }

    public void assertAllKeysHaveBeenSubscribedTo(Set<Integer> subscribedKeys) {
        for (int registeredKey : keyIdentifiers) {
            if(!subscribedKeys.contains(registeredKey))
            {
                throw new SpeedyMessagingInitiationException("Not all registered keys have been subscribed to");
            }
        }

    }

    public boolean containsRegistrationToKey(String key) {
        return keyIdentifiers.contains(key.hashCode());
    }

    public SubscriberChannelState getChannelSequenceState(int channelIdentifier) {
        return channelSequenceStates.get(channelIdentifier);
    }

    public void close() {
        channelSequenceStates.values().forEach(c -> c.close());
    }
}
