package io.kiw.speedy.helper;

import java.nio.ByteBuffer;

public class ByteBufferHelper {
    public static byte[] toByteArray(ByteBuffer message) {
        byte[] bytes = new byte[message.remaining()];
        message.get(bytes);
        return bytes;
    }
}
