package io.kiw.speedy.helper;

import java.util.Collection;
import java.util.Map;

public class ImmutableMapFactory {
    public static <T> ImmutableIntMap<T> initialiseIntMap(Map<Integer, T> integerMap)
    {
        InitalisationState<T> initalisationState = new InitalisationState<>();

        T[] underlyingArray;

        do {
            underlyingArray = initalisationState.buildUnderlyingArray();
            int mask = initalisationState.getMask();

            for (Map.Entry<Integer, T> entry : integerMap.entrySet()) {
                if(underlyingArray[entry.getKey() & mask] != null)
                {
                    initalisationState.seenCollision = true;
                    break;
                }
                else
                {
                    underlyingArray[entry.getKey() & mask] = entry.getValue();
                }
            }

        }
        while(initalisationState.seenCollisionAndDoubleUp());

        return new ImmutableIntMap<>(underlyingArray, integerMap.keySet(), integerMap.values());

    }

    public static <T> ImmutableLongMap<T> initialiseLongMap(Map<Long, T> longMap)
    {
        InitalisationState<T> initalisationState = new InitalisationState<>();

        T[] underlyingArray;

        do {
            underlyingArray = initalisationState.buildUnderlyingArray();
            int mask = initalisationState.getMask();

            for (Map.Entry<Long, T> entry : longMap.entrySet()) {
                if(underlyingArray[(int)(entry.getKey() & mask)] != null)
                {
                    initalisationState.seenCollision = true;
                    break;
                }
                else
                {
                    underlyingArray[(int)(entry.getKey() & mask)] = entry.getValue();
                }
            }

        }
        while(initalisationState.seenCollisionAndDoubleUp());
        Collection<T> values = longMap.values();
        T[] destinationValueArray = (T[])new Object[values.size()];
        int index = 0;
        for (T value : values) {
            destinationValueArray[index++] = value;
        }
        return new ImmutableLongMap<>(underlyingArray, longMap.keySet(), destinationValueArray);

    }

    private static class InitalisationState<T> {
        int size = 2;
        boolean seenCollision = false;

        public boolean seenCollisionAndDoubleUp() {
            final boolean seenCollision = this.seenCollision;
            if(seenCollision)
            {
                size = size * 2;
                this.seenCollision = false;
            }
            return seenCollision;
        }

        public int getMask()
        {
            return size - 1;
        }

        public T[] buildUnderlyingArray() {
            return (T[])new Object[size];
        }
    }
}
