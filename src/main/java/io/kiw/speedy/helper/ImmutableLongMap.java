package io.kiw.speedy.helper;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;
import java.util.function.Consumer;

public class ImmutableLongMap<T> {
    private final T[] underlyingArray;
    private final int mask;
    private final int size;
    private final Set<Long> keySet;
    private final T[] values;

    ImmutableLongMap(T[] underlyingArray, Set<Long> keySet, T[] values) {

        this.mask = underlyingArray.length - 1;
        this.underlyingArray = underlyingArray;
        this.keySet = Collections.unmodifiableSet(keySet);
        this.values = values;

        this.size = values.length;
    }

    public T get(long index)
    {
        return underlyingArray[(int)(index & mask)];
    }

    public void forEach(Consumer<T> consumer)
    {
        for (int i = 0; i < values.length; i++) {
            consumer.accept(values[i]);
        }
    }

    public boolean containsKey(long index) {
        return get(index) == null;
    }

    public int size() {
        return size;
    }

    public Set<Long> keySet() {
        return keySet;
    }
}
