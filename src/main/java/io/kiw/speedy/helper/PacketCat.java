package io.kiw.speedy.helper;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

public class PacketCat {
    private final static char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();


    public static String toPacketEventLog(ByteBuffer byteBuffer)
    {
        StringBuilder output = new StringBuilder();
        int originalLimit = byteBuffer.limit();
        int originalPosition = byteBuffer.position();

        byteBuffer.position(0);

        byte[] bytes = new byte[originalLimit];
        byteBuffer.get(bytes, 0, originalLimit);
        ByteBuffer heapBuffer = ByteBuffer.wrap(bytes);

        byteBuffer.position(originalPosition);

        try
        {
            long channelSequenceNumber = heapBuffer.getLong();

            output.append("-- NEW PACKET --\n");
            output.append("(Meta) Packet Size: ");
            output.append(originalLimit);
            output.append("\n");

            output.append("-- CHANNEL HEADER --\n");

            output.append("Channel Sequence Number: ");
            output.append(channelSequenceNumber);
            output.append("\n");

            int publisherIdentifier = heapBuffer.getInt();

            output.append("Publisher Identifier: ");
            output.append(publisherIdentifier);
            output.append("\n");

            int channelIdentifier = heapBuffer.getInt();


            output.append("Channel Identifier: ");
            output.append(channelIdentifier);
            output.append("\n");

            while(heapBuffer.limit() != heapBuffer.position())
            {
                parseEvent(output, heapBuffer);
            }
        }
        catch (Throwable t)
        {
            output.append("-- HIT CORRUPTED DATA. EXITING --");
        }
        finally {
            return output.toString();
        }

    }

    private static void parseEvent(StringBuilder output, ByteBuffer heapBuffer) {
        output.append("-- EVENT HEADER --\n");
        int eventKeyIdentifier = heapBuffer.getInt();
        output.append("Event Key Identifier: ");
        output.append(eventKeyIdentifier);
        output.append("\n");

        int remainingEventSize = heapBuffer.getInt();

        long eventSequenceNumber = heapBuffer.getLong();
        output.append("Event Sequence number: ");
        output.append(eventSequenceNumber);
        output.append("\n");

        output.append("Remaining Event Size: ");
        output.append(remainingEventSize);
        output.append("\n");
        output.append("(Meta) Remaining data on packet: ");
        output.append(heapBuffer.remaining());
        output.append("\n");

        int bytesInPAcketToParseForEvent = Math.min(heapBuffer.remaining(), remainingEventSize);
        byte[] eventBytes = new byte[bytesInPAcketToParseForEvent];
        heapBuffer.get(eventBytes, 0, bytesInPAcketToParseForEvent);

        output.append("-- EVENT --\n");

        output.append("Ascii: \n");
        output.append(new String(eventBytes, Charset.forName("UTF-8")));
        output.append("\n\n");

        output.append("Hex: \n");
        output.append(bytesToHex(eventBytes));
        output.append("\n\n");
    }

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[(bytes.length * 3) - 1];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            int hexIndex = j * 3;
            hexChars[hexIndex] = HEX_ARRAY[v >>> 4];
            hexChars[hexIndex + 1] = HEX_ARRAY[v & 0x0F];

            if(j != bytes.length -1)
            {
                hexChars[hexIndex + 2] = ' ';
            }
        }
        return new String(hexChars);
    }
}
