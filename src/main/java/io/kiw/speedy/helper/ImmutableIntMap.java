package io.kiw.speedy.helper;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

public class ImmutableIntMap<T> {
    private final T[] underlyingArray;
    private final int mask;
    private final int size;
    private final Set<Integer> keySet;
    private final Collection<T> values;

    ImmutableIntMap(T[] underlyingArray, Set<Integer> keySet, Collection<T> values) {

        this.mask = underlyingArray.length - 1;
        this.underlyingArray = underlyingArray;
        this.keySet = Collections.unmodifiableSet(keySet);
        this.values = Collections.unmodifiableCollection(values);
        this.size = values.size();
    }

    public T get(int index)
    {
        return underlyingArray[index & mask];
    }

    public Collection<T> values()
    {
        return values;
    }

    public boolean containsKey(int index) {
        return get(index) == null;
    }

    public int size() {
        return size;
    }

    public Set<Integer> keySet() {
        return keySet;
    }
}
