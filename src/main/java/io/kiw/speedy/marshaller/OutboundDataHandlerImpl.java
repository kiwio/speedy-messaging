package io.kiw.speedy.marshaller;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.channel.PublisherSequenceState;
import io.kiw.speedy.publisher.PacketFlusher;
import io.kiw.speedy.subscriber.SpeedyMessageHandler;
import io.kiw.speedy.wiring.SpeedyWiring;

import java.nio.ByteBuffer;

import static io.kiw.speedy.SpeedyMessagingImpl.DATAGRAM_LENGTH;

public class OutboundDataHandlerImpl implements OutboundDataHandler
{
    public static final int PACKET_HEADER_SIZE = (ByteSizes.LONG + ByteSizes.INTEGER + ByteSizes.INTEGER);
    public static final int MESSAGE_HEADER_SIZE = (ByteSizes.INTEGER + ByteSizes.INTEGER + ByteSizes.LONG);
    public static final int MAX_EVENT_DATA_SIZE_PER_PACKET = DATAGRAM_LENGTH - (MESSAGE_HEADER_SIZE + PACKET_HEADER_SIZE);
    public static final int MAX_EVENT_SIZE = MAX_EVENT_DATA_SIZE_PER_PACKET * 32;

    private final PacketFlusher packetflusher;
    private PublisherBucket[] publisherBuckets;
    private final SpeedyWiring wiring;
    private final EventMarshaller eventMarshaller;
    public OutboundDataHandlerImpl(PacketFlusher packetflusher, EventMarshaller eventMarshaller, PublisherBucket[] publisherBuckets, SpeedyWiring wiring) {

        this.packetflusher = packetflusher;
        this.eventMarshaller = eventMarshaller;
        this.publisherBuckets = publisherBuckets;
        this.wiring = wiring;
    }


    @Override
    public synchronized void handleData(String key, ByteBuffer data) {
        for (PublisherBucket publisherBucket : publisherBuckets) {
            if (publisherBucket.hasKey(key))
            {
                PublisherSequenceState packetChannelState = publisherBucket.getPublisherSequenceState();
                SpeedyConnection[] connectionsToPublishTo = publisherBucket.getRemoteConnections();

                eventMarshaller.push(
                        key,
                        data,
                        packetChannelState,
                        packetChannelState.getAndIncrementEventSequenceNumber(),
                        connectionsToPublishTo);
            }
        }
    }

    @Override
    public synchronized void resendData(PublisherBucket publisherBucket, long firstPacketKeyMissing, long lastPacketKeyMissing) {
        for (long i = firstPacketKeyMissing; i <= lastPacketKeyMissing; i++) {
            int eventId = wiring.startEvent();
            ByteBuffer message = publisherBucket.getPublisherSequenceState().recoverMessage(i);
            if(message != null)
            {
                this.packetflusher.flushResendMessage(message, publisherBucket.getRemoteConnections());
            }
            wiring.completeEvent(eventId);
        }

    }


    @Override
    public synchronized void flushDataIfRequired() {
        for (PublisherBucket publisherBucket : publisherBuckets) {
            ByteBuffer packetBuffer = publisherBucket.getPacketBuffer();
            if(packetBuffer.position() > PACKET_HEADER_SIZE)
            {
                this.packetflusher.flushNewMessage(packetBuffer, publisherBucket.getRemoteConnections(), publisherBucket.getPublisherSequenceState());
            }
        }
    }
}
