package io.kiw.speedy.marshaller;

public class ByteSizes {
    public static final int INTEGER = 4;
    public static final int IP_ADDRESS = 4;
    public static final int SHORT = 2;
    public static final int LONG = 8;
}
