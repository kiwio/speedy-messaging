package io.kiw.speedy.marshaller;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.subscriber.SpeedyMessageHandler;

public class MessageInstruction {
    private Type type;
    private String key;
    private byte[] data;
    private PublisherBucket publisherBucket;
    private SpeedyMessageHandler responseHandler;

    public MessageInstruction overwrite(MessageInstruction replacementInstance) {
        this.type = replacementInstance.type;
        this.key = replacementInstance.key;
        this.data = replacementInstance.data;
        this.publisherBucket = replacementInstance.publisherBucket;
        this.responseHandler = replacementInstance.responseHandler;
        return this;
    }

    public Type getType() {
        return type;
    }

    public String getKey() {
        return key;
    }

    public byte[] getData() {
        return data;
    }

    public PublisherBucket getPublisherBucket() {
        return publisherBucket;
    }

    public MessageInstruction overwrite(final Type type, String key, byte[] data, PublisherBucket publisherBucket, SpeedyMessageHandler responseHandler)
    {
        this.type = type;
        this.key = key;
        this.data = data;
        this.publisherBucket = publisherBucket;
        this.responseHandler = responseHandler;
        return this;
    }

    public SpeedyMessageHandler getResponseHandler() {
        return responseHandler;
    }

    public enum Type
    {
        SEND,
        FLUSH,
        RESEND,
        REQUEST
    }
}
