package io.kiw.speedy.marshaller;

import io.kiw.speedy.PublisherBucket;
import io.kiw.speedy.subscriber.SpeedyMessageHandler;

import java.nio.ByteBuffer;

public interface OutboundDataHandler {
    void resendData(PublisherBucket publisherBucket, long firstPacketKeyMissing, long lastPacketKeyMissing);

    void flushDataIfRequired();

    void handleData(String key, ByteBuffer data);
}
