package io.kiw.speedy.marshaller;

import io.kiw.speedy.SpeedyConnection;
import io.kiw.speedy.channel.PublisherSequenceState;
import io.kiw.speedy.publisher.PacketFlusher;

import java.nio.ByteBuffer;

import static io.kiw.speedy.SpeedyMessagingImpl.DATAGRAM_LENGTH;
import static io.kiw.speedy.marshaller.OutboundDataHandlerImpl.*;

public class EventMarshaller {
    private final PacketFlusher packetflusher;

    public EventMarshaller(PacketFlusher packetflusher) {

        this.packetflusher = packetflusher;
    }

    public void push(
            String key,
            ByteBuffer eventIntBytes,
            PublisherSequenceState packetSequenceState,
            final long eventSequenceNumber,
            SpeedyConnection[] connectionsToPublishTo
    )
    {
        int eventLength = eventIntBytes.limit();
        int packetsToSendCount = (eventLength + (MAX_EVENT_DATA_SIZE_PER_PACKET - 1)) / MAX_EVENT_DATA_SIZE_PER_PACKET; // ceil without doubles
        int remainingEventDataLength = eventLength;

        int offset = 0;

        ByteBuffer currentPublisherBuffer = packetSequenceState.getCurrentPublisherBuffer();

        for (int i = 0; i < packetsToSendCount; i++) {
            if (currentPublisherBuffer.remaining() != DATAGRAM_LENGTH - PACKET_HEADER_SIZE && remainingEventDataLength + MESSAGE_HEADER_SIZE > currentPublisherBuffer.remaining()) {
                this.packetflusher.flushNewMessage(currentPublisherBuffer, connectionsToPublishTo, packetSequenceState);
                currentPublisherBuffer = packetSequenceState.getCurrentPublisherBuffer();
            }

            int maxEventDataSizeInThisPacket = MAX_EVENT_DATA_SIZE_PER_PACKET;
            int writeLength = remainingEventDataLength < maxEventDataSizeInThisPacket ? remainingEventDataLength : maxEventDataSizeInThisPacket;
            MessageMarshaller.marshallIndividualMessage(key, eventIntBytes, offset, writeLength, remainingEventDataLength, currentPublisherBuffer, eventSequenceNumber);
            remainingEventDataLength -= writeLength;
            offset += writeLength;
        }
    }
}
