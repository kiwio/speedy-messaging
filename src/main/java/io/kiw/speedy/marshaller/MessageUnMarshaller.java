package io.kiw.speedy.marshaller;

import io.kiw.speedy.helper.ImmutableIntMap;
import io.kiw.speedy.publisher.PublishPromise;
import io.kiw.speedy.subscriber.OnMessageErrorHandler;
import io.kiw.speedy.subscriber.SpeedyMessageHandler;
import io.kiw.speedy.subscriber.SubscriberChannelState;

import java.nio.ByteBuffer;

public class MessageUnMarshaller
{

    public void unmarshallAndPotentiallyHandle(ByteBuffer packetBuffer,
                                               ImmutableIntMap<SpeedyMessageHandler> subscriptions,
                                               OnMessageErrorHandler onMessageErrorHandler,
                                               SubscriberChannelState channelState,
                                               int publisherIdentifier,
                                               PublishPromise publishPromise) {

        ByteBuffer eventBuffer = channelState.getEventBuffer();
        int keyIdentifier = packetBuffer.getInt();
        int remainingDataLengthForBlock = packetBuffer.getInt();

        channelState.setEventSequenceNumber(packetBuffer.getLong());
        int lengthOfApplicationDataInPacket = Math.min(remainingDataLengthForBlock, packetBuffer.remaining());


        int oldLimit = packetBuffer.limit();
        packetBuffer.limit(packetBuffer.position() + lengthOfApplicationDataInPacket);
        eventBuffer.put(packetBuffer);
        packetBuffer.limit(oldLimit);

        if (remainingDataLengthForBlock - lengthOfApplicationDataInPacket <= 0) // if is a complete message
        {
            eventBuffer.flip();
            SpeedyMessageHandler speedyMessageHandler = subscriptions.get(keyIdentifier);
            if(speedyMessageHandler != null)
            {
                try
                {
                    channelState.handleMessage(speedyMessageHandler, eventBuffer, publisherIdentifier, publishPromise);
                }
                catch (Throwable t)
                {
                    onMessageErrorHandler.handle(t);
                }
                finally {
                    eventBuffer.clear();
                }
            }
            else
            {
                System.out.println("Unknown key " + keyIdentifier);
            }
        }
    }
}
