package io.kiw.speedy.marshaller;


import java.nio.ByteBuffer;

public class MessageMarshaller {
    public static void marshallIndividualMessage(String key, ByteBuffer allApplicationData, int offset, int writeLength, int remainingEventDataLength, ByteBuffer packetBuffer, long eventSequenceNumber) {
        packetBuffer.putInt(key.hashCode()); // key identifier
        packetBuffer.putInt(remainingEventDataLength);
        packetBuffer.putLong(eventSequenceNumber);
        allApplicationData.position(offset);
        allApplicationData.limit(offset + writeLength);
        packetBuffer.put(allApplicationData);
    }
}
