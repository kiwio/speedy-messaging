package io.kiw.speedy.execution;

public interface SpeedyExecution {
    void run(byte[] data);
}
