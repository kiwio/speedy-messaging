package io.kiw.speedy;

import io.kiw.speedy.marshaller.ByteSizes;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.util.Arrays;

public class SpeedyHost {
    private final String name;
    private final byte[] ipAddressAsBytes;
    private final int port;
    private final InetAddress inetAddress;
    private final byte[] marshalled;
    private final InetSocketAddress inetSocketAddress;
    private final int ipAddressAsInt;

    public SpeedyHost(final String name, String ipAddress, int port) {
        this.name = name;
        this.ipAddressAsBytes = deriveIpAddressBytesByString(ipAddress);
        this.inetAddress = deriveInetAddressFromString(ipAddress);
        this.ipAddressAsInt = ipAddressBytesToInt(ipAddressAsBytes); // actually ip address as int
        this.port = port;
        this.marshalled = marshall(name, ipAddressAsBytes, port);
        inetSocketAddress = new InetSocketAddress(inetAddress, port);
    }

    public static int ipAddressBytesToInt(byte[] ipAddressAsBytes) {
        return ipAddressAsBytes[0] << 24 + ipAddressAsBytes[1] << 16 + ipAddressAsBytes[2] << 8 + ipAddressAsBytes[3];
    }

    public SpeedyHost(String name, byte[] ipAddressBytes, int port) {
        this.name = name;
        this.ipAddressAsBytes = ipAddressBytes;
        this.inetAddress = deriveInetAddressFromBytes(ipAddressAsBytes);
        this.ipAddressAsInt = ipAddressBytesToInt(ipAddressAsBytes); // actually ip address as int
        this.port = port;
        this.marshalled = marshall(name, ipAddressAsBytes, port);
        inetSocketAddress = new InetSocketAddress(inetAddress, port);
    }

    public String getName() {
        return name;
    }

    public int getPort() {
        return port;
    }

    public byte[] getIpAddressAsBytes() {
        return ipAddressAsBytes;
    }

    private static byte[] deriveIpAddressBytesByString(String ipAddress) {
        try {
            return InetAddress.getByName(ipAddress).getAddress();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public int getIpAddressAsInt() {
        return ipAddressAsInt;
    }

    private InetAddress deriveInetAddressFromBytes(byte[] ipAddressAsBytes) {
        try {
            return InetAddress.getByAddress(ipAddressAsBytes);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
    private static InetAddress deriveInetAddressFromString(String ipAddress) {
        try {
            return InetAddress.getByName(ipAddress);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public byte[] marshalled() {
        return marshalled;
    }

    private static byte[] marshall(String name, byte[] ipAddressAsBytes, int port) {
        byte[] nameAsBytes = name.getBytes();
        return ByteBuffer.allocate((ByteSizes.IP_ADDRESS + ByteSizes.INTEGER) + nameAsBytes.length).
                put(nameAsBytes).
                put(ipAddressAsBytes).
                putInt(port).array();
    }

    public static SpeedyHost unmarshall(byte[] message) {
        ByteBuffer wrapped = ByteBuffer.wrap(message);
        int nameLength = message.length - (ByteSizes.IP_ADDRESS + ByteSizes.INTEGER);
        byte[] nameAsBytes = new byte[nameLength];
        wrapped.get(nameAsBytes, 0, nameLength);
        byte[] ipAddressBytes = new byte[ByteSizes.IP_ADDRESS];
        wrapped.get(ipAddressBytes, 0, ByteSizes.IP_ADDRESS);
        int port = wrapped.getInt();

        return new SpeedyHost(new String(nameAsBytes), ipAddressBytes, port);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SpeedyHost that = (SpeedyHost) o;

        if (port != that.port) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return Arrays.equals(ipAddressAsBytes, that.ipAddressAsBytes);

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(ipAddressAsBytes);
        result = 31 * result + port;
        return result;
    }

    public SocketAddress getSocketAddress() {
        return inetSocketAddress;
    }

    @Override
    public String toString() {
        return "SpeedyHost{" +
                "name='" + name + '\'' +
                ", port=" + port +
                ", inetAddress=" + inetAddress +
                '}';
    }
}
